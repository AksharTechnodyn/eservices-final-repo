﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using eServices.Models;
using System.Net.Mail;
using System.Configuration;
using System.Net;

namespace eServices
{
    public class EmailService : IIdentityMessageService
    {
        ApplicationDbContext db = new ApplicationDbContext();
        public Task SendAsync(IdentityMessage message)
        {
            // Plug in your email service here to send an email.
            return Task.FromResult(0);
        }

        public Task Forgot(string message, string email)
        {
            return Task.Factory.StartNew(() =>
            {
                ForgotPassword(message, email);
            });
        }

        public Task Verify(string Email, string accountnumber, string result)
        {
            return Task.Factory.StartNew(() =>
            {
                VerifyAccount(Email, accountnumber, result);
            });
        }

        public Task VerifyReject(string Email, string accountnumber, string result, string reason)
        {
            return Task.Factory.StartNew(() =>
            {
                VerifyAccountReject(Email, accountnumber, result, reason);
            });
        }

        public void SendStatement(string accountnumber)
        {
            var obj = db.LinkedAccounts.ToList().Find(x => x.AccountNumber == accountnumber).AccountId;
            List<LinkedAccountEmail> emails = db.LinkedAccountEmails.ToList().FindAll(x => x.AccountId == obj);

            if (emails.Count > 0)
            {
                StatementSend(accountnumber);
            }

        }

        public void VerifyAccount(string Email, string accountnumber, string result)
        {
            #region formatter
            var subject = "eServices - Account Verification";
            var obj = db.LinkedAccounts.ToList().Find(x => x.AccountNumber == accountnumber).AccountId;
            List<LinkedAccountEmail> emails = db.LinkedAccountEmails.ToList().FindAll(x => x.AccountId == obj);
                        
            var bodydata = "Dear Customer,  <br/><br/>" + " Please be advised that account with account number <b> " + accountnumber + "</b>" + " has been " + result + "." + "<br/>Kind Regards, <br/><br/> eThekwini Municipality. <br/></br> 031 328 2500 / 067 214 3732";
            #endregion

            MailMessage msg = new MailMessage();
            msg.From = new MailAddress(ConfigurationManager.AppSettings["Email"].ToString());
            //msg.To.Add(new MailAddress(to));
            msg.To.Add(Email);
            msg.Subject = subject;
            msg.Body = bodydata;
            msg.IsBodyHtml = true;

            try
            {
                using (var smtp = new SmtpClient())
                {
                    var credential = new NetworkCredential
                    {
                        UserName = "no-reply@coralite.co.za",
                        Password = "Noreply01#"
                    };
                    smtp.Credentials = credential;
                    smtp.Host = "mail.coralite.co.za";
                    smtp.Port = 587;
                    smtp.EnableSsl = false;
                    smtp.Send(msg);
                }
            }
            catch
            {
                return;
            }
        }

        public void VerifyAccountReject(string Email, string accountnumber, string result,string reason)
        {
            #region formatter
            var subject = "eServices - Account Verification";
            var obj = db.LinkedAccounts.ToList().Find(x => x.AccountNumber == accountnumber).AccountId;
            List<LinkedAccountEmail> emails = db.LinkedAccountEmails.ToList().FindAll(x => x.AccountId == obj);

            var ln = db.LinkedAccounts.ToList().Find(x => x.AccountNumber == accountnumber).AccountId;
            var rs = db.RejectReasons.ToList().FindAll(x => x.ReasonId == ln);

            var bodydata = "Dear Customer,  <br/><br/>" + " Please be advised that account with account number <b> " + accountnumber + "</b>" + " has been " + result + "." + " The reason for rejecting the request is <b> " + reason + "</b>" + "<br/>Kind Regards, <br/><br/> eThekwini Municipality. <br/></br> 031 328 2500 / 067 214 3732";
            #endregion

            MailMessage msg = new MailMessage();
            msg.From = new MailAddress(ConfigurationManager.AppSettings["Email"].ToString());
            //msg.To.Add(new MailAddress(to));
            msg.To.Add(Email);
            msg.Subject = subject;
            msg.Body = bodydata;
            msg.IsBodyHtml = true;

            try
            {
                using (var smtp = new SmtpClient())
                {
                    var credential = new NetworkCredential
                    {
                        UserName = "no-reply@coralite.co.za",
                        Password = "Noreply01#"
                    };
                    smtp.Credentials = credential;
                    smtp.Host = "mail.coralite.co.za";
                    smtp.Port = 587;
                    smtp.EnableSsl = false;
                    smtp.Send(msg);
                }
            }
            catch
            {
                return;
            }
        }


        public void StatementSend(string accountnumber)
        {
            #region formatter
            var subject = "eServices - Monthly Bill Statement";
            var obj = db.LinkedAccounts.ToList().Find(x => x.AccountNumber == accountnumber).AccountId;
            List<LinkedAccountEmail> emails = db.LinkedAccountEmails.ToList().FindAll(x => x.AccountId == obj);

            var bodydata = "Dear Customer,  <br/><br/>" + " Please find an attachment of your bill for the current month. <br/>Kind Regards, <br/><br/> eThekwini Municipality. <br/></br> 031 328 2500 / 067 214 3732";
            #endregion

            MailMessage msg = new MailMessage();
            msg.From = new MailAddress(ConfigurationManager.AppSettings["Email"].ToString());
            //msg.To.Add(new MailAddress(to));
            foreach (var e in emails)
            {
                msg.To.Add(new MailAddress(e.LinkedEmail));
            }
            msg.Subject = subject;
            msg.Body = bodydata;
            msg.IsBodyHtml = true;

            string path = System.IO.Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/Content"), "Bill (26).pdf");

            System.Net.Mail.Attachment attachment;
            attachment = new System.Net.Mail.Attachment(path);
            msg.Attachments.Add(attachment);
            try
            {
                using (var smtp = new SmtpClient())
                {
                    var credential = new NetworkCredential
                    {
                        UserName = "no-reply@coralite.co.za",
                        Password = "Noreply01#"
                    };
                    smtp.Credentials = credential;
                    smtp.Host = "mail.coralite.co.za";
                    smtp.Port = 587;
                    smtp.EnableSsl = false;
                    smtp.Send(msg);
                }
            }
            catch
            {
                return;
            }

        }

        void ForgotPassword(string message, string email)
        {
            #region formatter
            //var bodydata = "Please confirm your account by clicking this link: <a href=\"" + message.Body + "\">link</a><br/>";
            //string text = string.Format("Please click on this link to {0}: {1}", message.Subject, message.Body);
            //string html = "Please confirm your account by clicking this link: <a href=\"" + message.Body + "\">link</a><br/>";

            //html += HttpUtility.HtmlEncode(@"Or click on the copy the following link on the browser:" + message.Body);
            #endregion

            MailMessage msg = new MailMessage();
            msg.From = new MailAddress(ConfigurationManager.AppSettings["Email"].ToString());
            msg.To.Add(new MailAddress(email));
            msg.Subject = "eServices - Password Reset";
            msg.Body = message;
            msg.IsBodyHtml = true;
            //msg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(text, null, MediaTypeNames.Text.Plain));
            //msg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(html, null, MediaTypeNames.Text.Html));

            SmtpClient smtpClient = new SmtpClient("mail.coralite.co.za", Convert.ToInt32(587));
            System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["Email"].ToString(), ConfigurationManager.AppSettings["Password"].ToString());
            smtpClient.Credentials = credentials;
            smtpClient.EnableSsl = false;
            smtpClient.Send(msg);
        }
    }



    public class SmsService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            // Plug in your SMS service here to send a text message.
            return Task.FromResult(0);
        }
    }

    // Configure the application user manager used in this application. UserManager is defined in ASP.NET Identity and is used by the application.
    public class ApplicationUserManager : UserManager<ApplicationUser>
    {
        public ApplicationUserManager(IUserStore<ApplicationUser> store)
            : base(store)
        {
        }

        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
        {
            var manager = new ApplicationUserManager(new UserStore<ApplicationUser>(context.Get<ApplicationDbContext>()));
            // Configure validation logic for usernames
            manager.UserValidator = new UserValidator<ApplicationUser>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };

            // Configure validation logic for passwords
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = true,
                RequireDigit = true,
                RequireLowercase = true,
                RequireUppercase = true,
            };

            // Configure user lockout defaults
            manager.UserLockoutEnabledByDefault = true;
            manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
            manager.MaxFailedAccessAttemptsBeforeLockout = 5;

            // Register two factor authentication providers. This application uses Phone and Emails as a step of receiving a code for verifying the user
            // You can write your own provider and plug it in here.
            manager.RegisterTwoFactorProvider("Phone Code", new PhoneNumberTokenProvider<ApplicationUser>
            {
                MessageFormat = "Your security code is {0}"
            });
            manager.RegisterTwoFactorProvider("Email Code", new EmailTokenProvider<ApplicationUser>
            {
                Subject = "Security Code",
                BodyFormat = "Your security code is {0}"
            });
            manager.EmailService = new EmailService();
            manager.SmsService = new SmsService();
            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider =
                    new DataProtectorTokenProvider<ApplicationUser>(dataProtectionProvider.Create("ASP.NET Identity"));
            }
            return manager;
        }

        internal string GeneratePasswordResetTokenAsync(int id)
        {
            throw new NotImplementedException();
        }
    }

    // Configure the application sign-in manager which is used in this application.
    public class ApplicationSignInManager : SignInManager<ApplicationUser, string>
    {
        public ApplicationSignInManager(ApplicationUserManager userManager, IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        {
        }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(ApplicationUser user)
        {
            return user.GenerateUserIdentityAsync((ApplicationUserManager)UserManager);
        }

        public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options, IOwinContext context)
        {
            return new ApplicationSignInManager(context.GetUserManager<ApplicationUserManager>(), context.Authentication);
        }
    }
}
