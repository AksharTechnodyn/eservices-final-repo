namespace eServices.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class finalMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AccountLinkRequests",
                c => new
                    {
                        accountRequestID = c.String(nullable: false, maxLength: 128),
                        CustomerId = c.String(maxLength: 128),
                        AccountType = c.String(),
                        AccountNumber = c.String(nullable: false),
                        AccountStatus = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.accountRequestID)
                .ForeignKey("dbo.Customers", t => t.CustomerId)
                .Index(t => t.CustomerId);
            
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        CustomerId = c.String(nullable: false, maxLength: 128),
                        CustomerName = c.String(nullable: false),
                        CustomerSurname = c.String(nullable: false),
                        Email = c.String(nullable: false),
                        TypeofId = c.String(),
                        IdNumber = c.String(maxLength: 13),
                        PassportNumber = c.String(),
                        Gender = c.String(nullable: false),
                        CellphoneNumber = c.String(nullable: false, maxLength: 10),
                        PhysicalAddress = c.String(nullable: false),
                        PhysicalAddressLine2 = c.String(),
                        PhysicalPostalCode = c.String(nullable: false, maxLength: 4),
                        PostalAddress = c.String(nullable: false),
                        PostalAddressLine2 = c.String(),
                        PostalAddressCode = c.String(nullable: false, maxLength: 4),
                        RoleName = c.String(),
                    })
                .PrimaryKey(t => t.CustomerId);
            
            CreateTable(
                "dbo.CustomerDocuments",
                c => new
                    {
                        DocumentsId = c.String(nullable: false, maxLength: 128),
                        uploadDate = c.DateTime(nullable: false),
                        Documents = c.String(),
                        CustomerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.DocumentsId)
                .ForeignKey("dbo.Customers", t => t.CustomerId)
                .Index(t => t.CustomerId);
            
            CreateTable(
                "dbo.LinkedAccounts",
                c => new
                    {
                        AccountId = c.String(nullable: false, maxLength: 128),
                        CustomerId = c.String(maxLength: 128),
                        AccountType = c.String(),
                        ReasonId = c.String(maxLength: 128),
                        AccountNumber = c.String(nullable: false),
                        AccountStatus = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                        Documents = c.String(),
                        AccountType_AccountTypeId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.AccountId)
                .ForeignKey("dbo.Customers", t => t.CustomerId)
                .ForeignKey("dbo.RejectReasons", t => t.ReasonId)
                .ForeignKey("dbo.AccountTypes", t => t.AccountType_AccountTypeId)
                .Index(t => t.CustomerId)
                .Index(t => t.ReasonId)
                .Index(t => t.AccountType_AccountTypeId);
            
            CreateTable(
                "dbo.LinkedAccountEmails",
                c => new
                    {
                        EmailId = c.String(nullable: false, maxLength: 128),
                        AccountId = c.String(maxLength: 128),
                        LinkedEmail = c.String(),
                        EmailStatus = c.String(),
                    })
                .PrimaryKey(t => t.EmailId)
                .ForeignKey("dbo.LinkedAccounts", t => t.AccountId)
                .Index(t => t.AccountId);
            
            CreateTable(
                "dbo.RejectReasons",
                c => new
                    {
                        ReasonId = c.String(nullable: false, maxLength: 128),
                        Description = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ReasonId);
            
            CreateTable(
                "dbo.AccountTypes",
                c => new
                    {
                        AccountTypeId = c.String(nullable: false, maxLength: 128),
                        AccountTypeName = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.AccountTypeId);
            
            CreateTable(
                "dbo.Bills",
                c => new
                    {
                        BillId = c.String(nullable: false, maxLength: 128),
                        InvoiceNumber = c.String(nullable: false),
                        AccountHolderName = c.String(nullable: false),
                        DateAccountDue = c.String(nullable: false),
                        BillPeriod = c.String(nullable: false),
                        AccountNumber = c.String(nullable: false),
                        PreviousBalance = c.String(nullable: false),
                        PaymentMade = c.String(nullable: false),
                        PaymentMadeAmount = c.String(),
                        ChargesAmount = c.String(),
                        BalanceBroughtForward = c.String(nullable: false),
                        StandSize = c.String(nullable: false),
                        DateofValuation = c.String(nullable: false),
                        MunicipalValuation = c.String(nullable: false),
                        TotalAmountDue = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.BillId);
            
            CreateTable(
                "dbo.Charges",
                c => new
                    {
                        ChargesId = c.String(nullable: false, maxLength: 128),
                        BillId = c.String(maxLength: 128),
                        ItemName = c.String(nullable: false),
                        ItemGroup = c.String(),
                        Price = c.Double(nullable: false),
                        Date = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ChargesId)
                .ForeignKey("dbo.Bills", t => t.BillId)
                .Index(t => t.BillId);
            
            CreateTable(
                "dbo.PropertyValuations",
                c => new
                    {
                        ChargesId = c.String(nullable: false, maxLength: 128),
                        BillId = c.String(maxLength: 128),
                        StandSize = c.String(nullable: false),
                        DateofValuation = c.String(nullable: false),
                        MunicipalValuation = c.String(nullable: false),
                        Date = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ChargesId)
                .ForeignKey("dbo.Bills", t => t.BillId)
                .Index(t => t.BillId);
            
            CreateTable(
                "dbo.CorrespondenceCustomers",
                c => new
                    {
                        CustomerId = c.String(nullable: false, maxLength: 128),
                        CustomerName = c.String(nullable: false),
                        CustomerSurname = c.String(nullable: false),
                        Email = c.String(nullable: false),
                        IdNumber = c.String(maxLength: 13),
                        PassportNumber = c.String(),
                        CellphoneNumber = c.String(nullable: false, maxLength: 10),
                        PhysicalAddress = c.String(nullable: false),
                        PhysicalAddressLine2 = c.String(),
                        PhysicalPostalCode = c.String(nullable: false, maxLength: 4),
                        PostalAddress = c.String(nullable: false),
                        PostalAddressLine2 = c.String(),
                        PostalAddressCode = c.String(nullable: false, maxLength: 4),
                    })
                .PrimaryKey(t => t.CustomerId);
            
            CreateTable(
                "dbo.CorrespondenceDocuments",
                c => new
                    {
                        DocumentsId = c.String(nullable: false, maxLength: 128),
                        uploadDate = c.DateTime(nullable: false),
                        Documents = c.String(),
                    })
                .PrimaryKey(t => t.DocumentsId);
            
            CreateTable(
                "dbo.CorrespondenceReasons",
                c => new
                    {
                        ReasonId = c.String(nullable: false, maxLength: 128),
                        CorrespondenceReasonName = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ReasonId);
            
            CreateTable(
                "dbo.Correspondences",
                c => new
                    {
                        CorrespondenceId = c.String(nullable: false, maxLength: 128),
                        CustomerId = c.String(maxLength: 128),
                        DocumentId = c.String(),
                        TypeId = c.String(maxLength: 128),
                        ReasonId = c.String(maxLength: 128),
                        UnitId = c.String(maxLength: 128),
                        Details = c.String(nullable: false),
                        CorrespondenceDocument_DocumentsId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.CorrespondenceId)
                .ForeignKey("dbo.CorrespondenceCustomers", t => t.CustomerId)
                .ForeignKey("dbo.CorrespondenceDocuments", t => t.CorrespondenceDocument_DocumentsId)
                .ForeignKey("dbo.CorrespondenceReasons", t => t.ReasonId)
                .ForeignKey("dbo.CorrespondenceTypes", t => t.TypeId)
                .ForeignKey("dbo.MunicipalUnits", t => t.UnitId)
                .Index(t => t.CustomerId)
                .Index(t => t.TypeId)
                .Index(t => t.ReasonId)
                .Index(t => t.UnitId)
                .Index(t => t.CorrespondenceDocument_DocumentsId);
            
            CreateTable(
                "dbo.CorrespondenceTypes",
                c => new
                    {
                        TypeId = c.String(nullable: false, maxLength: 128),
                        CorrespondenceTypeName = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.TypeId);
            
            CreateTable(
                "dbo.MunicipalUnits",
                c => new
                    {
                        UnitId = c.String(nullable: false, maxLength: 128),
                        MunicipalUnitName = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.UnitId);
            
            CreateTable(
                "dbo.LinkedAccountUploads",
                c => new
                    {
                        DocumentsId = c.String(nullable: false, maxLength: 128),
                        uploadDate = c.DateTime(nullable: false),
                        Documents = c.String(),
                        CustomerId = c.String(maxLength: 128),
                        AccountId = c.String(),
                    })
                .PrimaryKey(t => t.DocumentsId)
                .ForeignKey("dbo.Customers", t => t.CustomerId)
                .Index(t => t.CustomerId);
            
            CreateTable(
                "dbo.moqLinkedAccounts",
                c => new
                    {
                        AccountId = c.String(nullable: false, maxLength: 128),
                        AccountType = c.String(),
                        AccountName = c.String(),
                        AccountNumber = c.String(),
                        AccountStatus = c.String(),
                        DateCreated = c.DateTime(),
                    })
                .PrimaryKey(t => t.AccountId);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.LinkedAccountUploads", "CustomerId", "dbo.Customers");
            DropForeignKey("dbo.Correspondences", "UnitId", "dbo.MunicipalUnits");
            DropForeignKey("dbo.Correspondences", "TypeId", "dbo.CorrespondenceTypes");
            DropForeignKey("dbo.Correspondences", "ReasonId", "dbo.CorrespondenceReasons");
            DropForeignKey("dbo.Correspondences", "CorrespondenceDocument_DocumentsId", "dbo.CorrespondenceDocuments");
            DropForeignKey("dbo.Correspondences", "CustomerId", "dbo.CorrespondenceCustomers");
            DropForeignKey("dbo.PropertyValuations", "BillId", "dbo.Bills");
            DropForeignKey("dbo.Charges", "BillId", "dbo.Bills");
            DropForeignKey("dbo.LinkedAccounts", "AccountType_AccountTypeId", "dbo.AccountTypes");
            DropForeignKey("dbo.AccountLinkRequests", "CustomerId", "dbo.Customers");
            DropForeignKey("dbo.LinkedAccounts", "ReasonId", "dbo.RejectReasons");
            DropForeignKey("dbo.LinkedAccountEmails", "AccountId", "dbo.LinkedAccounts");
            DropForeignKey("dbo.LinkedAccounts", "CustomerId", "dbo.Customers");
            DropForeignKey("dbo.CustomerDocuments", "CustomerId", "dbo.Customers");
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.LinkedAccountUploads", new[] { "CustomerId" });
            DropIndex("dbo.Correspondences", new[] { "CorrespondenceDocument_DocumentsId" });
            DropIndex("dbo.Correspondences", new[] { "UnitId" });
            DropIndex("dbo.Correspondences", new[] { "ReasonId" });
            DropIndex("dbo.Correspondences", new[] { "TypeId" });
            DropIndex("dbo.Correspondences", new[] { "CustomerId" });
            DropIndex("dbo.PropertyValuations", new[] { "BillId" });
            DropIndex("dbo.Charges", new[] { "BillId" });
            DropIndex("dbo.LinkedAccountEmails", new[] { "AccountId" });
            DropIndex("dbo.LinkedAccounts", new[] { "AccountType_AccountTypeId" });
            DropIndex("dbo.LinkedAccounts", new[] { "ReasonId" });
            DropIndex("dbo.LinkedAccounts", new[] { "CustomerId" });
            DropIndex("dbo.CustomerDocuments", new[] { "CustomerId" });
            DropIndex("dbo.AccountLinkRequests", new[] { "CustomerId" });
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.moqLinkedAccounts");
            DropTable("dbo.LinkedAccountUploads");
            DropTable("dbo.MunicipalUnits");
            DropTable("dbo.CorrespondenceTypes");
            DropTable("dbo.Correspondences");
            DropTable("dbo.CorrespondenceReasons");
            DropTable("dbo.CorrespondenceDocuments");
            DropTable("dbo.CorrespondenceCustomers");
            DropTable("dbo.PropertyValuations");
            DropTable("dbo.Charges");
            DropTable("dbo.Bills");
            DropTable("dbo.AccountTypes");
            DropTable("dbo.RejectReasons");
            DropTable("dbo.LinkedAccountEmails");
            DropTable("dbo.LinkedAccounts");
            DropTable("dbo.CustomerDocuments");
            DropTable("dbo.Customers");
            DropTable("dbo.AccountLinkRequests");
        }
    }
}
