﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(eServices.Startup))]
namespace eServices
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
