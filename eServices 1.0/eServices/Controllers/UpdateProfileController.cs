using eServices.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace eServices.Controllers
{
    public class UpdateProfileController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: UpdateProfile
        public ActionResult Index()
        {
            return View();
        }


        // GET: Customers/Edit/5
        public ActionResult Edit()
        {
            string id = db.Customers.Where(x => x.Email == System.Web.HttpContext.Current.User.Identity.Name).FirstOrDefault().CustomerId;

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer customer = db.Customers.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            ViewBag.show = "none";
            ViewBag.show1 = "none";
            if (customer.PassportNumber ==null)
            {
                customer.IdNumber = customer.IdNumber;
                ViewBag.show = "show";
                ViewBag.show1 = "none";
            }
            else
            {
                customer.PassportNumber = customer.PassportNumber;
                ViewBag.show1 = "show";
                ViewBag.show = "none";
            }

            return View(customer);
        }



        // POST: Customers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CustomerId,CustomerName,CustomerSurname,Email,IdNumber,PassportNumber,Gender,CellphoneNumber,PhysicalAddress,PhysicalAddressLine2,PhysicalPostalCode,PostalAddress,PostalAddressLine2,PostalAddressCode")] Customer customer)
        {
          
            if (ModelState.IsValid)
            {
                try
                {
                    //if (customer.Month == null)
                    //{
                    //    TempData["AlertMessage"] = "Please select month";
                    //    return View(customer);
                    //}
                    customer.CellphoneNumber = customer.CellphoneNumber;
                    //customer.Password = customer.Password;
                    //customer.ConfirmPassword = customer.ConfirmPassword;
                    customer.CustomerName = customer.CustomerName;
                    customer.CustomerSurname = customer.CustomerSurname;
                    customer.Email = customer.Email;
                    customer.IdNumber = customer.IdNumber;
                    customer.PassportNumber = customer.PassportNumber;





                    //if (customer.TypeofId==customer.IdNumber)
                    //{
                    //    customer.IdNumber = customer.IdNumber;
                    //    customer.PassportNumber = null;
                    //}
                    //else
                    //{
                    //    customer.IdNumber = null;
                    //    customer.PassportNumber = customer.PassportNumber;
                    //}
                    customer.Gender = customer.Gender;
                    customer.PhysicalAddress = customer.PhysicalAddress;
                    customer.PhysicalAddressLine2 = customer.PhysicalAddressLine2;
                    customer.PhysicalPostalCode = customer.PhysicalPostalCode;
                    customer.PostalAddress = customer.PostalAddress;
                    customer.PostalAddressCode = customer.PostalAddressCode;
                    customer.PostalAddressLine2 = customer.PostalAddressLine2;
                    //customer.TypeofId = customer.TypeofId;
                    db.Entry(customer).State = EntityState.Modified;
                    db.SaveChanges();
                    TempData["AlertMessage"] = "Profile updated";
                    return RedirectToAction("Edit", "UpdateProfile");
                }
                catch (Exception ex)
                {
                    //Log the error (uncomment dex variable name and add a line here to write a log.
                    ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists, see your system administrator.");
                }
            }
            return View(customer);
        }

    }
}