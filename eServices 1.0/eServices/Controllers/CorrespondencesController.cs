﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using eServices.Models;

namespace eServices.Controllers
{
    public class CorrespondencesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Correspondences
        public ActionResult Index()
        {
            var correspondences = db.Correspondences.Include(c => c.CorrespondenceCustomer).Include(c => c.CorrespondenceReason).Include(c => c.MunicipalUnit);
            return View(correspondences.ToList());
        }

        // GET: Correspondences/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Correspondence correspondence = db.Correspondences.Find(id);
            if (correspondence == null)
            {
                return HttpNotFound();
            }
            return View(correspondence);
        }

        // GET: Correspondences/CustomerDetails/5
        public ActionResult CustomerDetails(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CorrespondenceCustomer correspondence = db.CorrespondenceCustomers.Find(id);
            if (correspondence == null)
            {
                return HttpNotFound();
            }
            return View(correspondence);
        }

            // GET: Correspondences/Create
            public ActionResult Create()
        {
            Customer model = db.Customers.ToList().Find(m=>m.Email == User.Identity.Name);
            CorrespondenceViewModel obj = new CorrespondenceViewModel
            {
                CustomerId = model.CustomerId,
                CustomerName = model.CustomerName,
                CustomerSurname = model.CustomerSurname,
                Email = model.Email,
                IdNumber = model.IdNumber,
                CellphoneNumber = model.CellphoneNumber,
                PhysicalAddress = model.PhysicalAddress,
                PhysicalAddressLine2 = model.PhysicalAddressLine2,
                PhysicalPostalCode = model.PhysicalPostalCode,
                PostalAddress = model.PostalAddress,
                PostalAddressLine2 = model.PostalAddressLine2,
                PostalAddressCode = model.PostalAddressCode
            };
            ViewBag.TypeId = new SelectList(db.CorrespondenceTypes, "TypeId", "CorrespondenceTypeName");
            ViewBag.ReasonId = new SelectList(db.CorrespondenceReasons, "ReasonId", "CorrespondenceReasonName");
            ViewBag.UnitId = new SelectList(db.MunicipalUnits, "UnitId", "MunicipalUnitName");
            return View(obj);
        }

        // POST: Correspondences/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CorrespondenceViewModel correspondenceViewModel)
        {
            if (ModelState.IsValid)
            {
                var id = SaveCustomer(correspondenceViewModel);
                var doc = SaveDocument(correspondenceViewModel);
                Correspondence correspondence = new Correspondence
                {
                    CorrespondenceId = Guid.NewGuid().ToString(),
                    CustomerId = id,
                    DocumentId = doc,
                    Details = correspondenceViewModel.Details,
                    UnitId = correspondenceViewModel.UnitId,
                    ReasonId = correspondenceViewModel.ReasonId,
                };
                db.Correspondences.Add(correspondence);
                db.SaveChanges();
                TempData["AlertMessage"] = "Correspondence Submitted!";
                return RedirectToAction("Index","Home");
            }

            ViewBag.TypeId = new SelectList(db.CorrespondenceTypes, "TypeId", "CorrespondenceTypeName", correspondenceViewModel.TypeId);
            ViewBag.ReasonId = new SelectList(db.CorrespondenceReasons, "ReasonId", "CorrespondenceReasonName", correspondenceViewModel.ReasonId);
            ViewBag.UnitId = new SelectList(db.MunicipalUnits, "UnitId", "MunicipalUnitName", correspondenceViewModel.UnitId);
            return View(correspondenceViewModel);
        }


        public string SaveDocument(CorrespondenceViewModel model)
        {

            CorrespondenceDocument obj = new CorrespondenceDocument
            {
                DocumentsId = Guid.NewGuid().ToString(),
                Documents = model.Documents,
                uploadDate = DateTime.Now
            };

            db.CorrespondenceDocuments.Add(obj);
            db.SaveChanges();

            return obj.DocumentsId;
        }
        public string SaveCustomer(CorrespondenceViewModel model)
        {

            CorrespondenceCustomer obj = new CorrespondenceCustomer
            {
                CustomerId = Guid.NewGuid().ToString(),
                CustomerName = model.CustomerName,
                CustomerSurname = model.CustomerSurname,
                Email = model.Email,
                IdNumber = model.IdNumber,
                CellphoneNumber = model.CellphoneNumber,
                PhysicalAddress = model.PhysicalAddress,
                PhysicalAddressLine2 = model.PhysicalAddressLine2,
                PhysicalPostalCode = model.PhysicalPostalCode,
                PostalAddress = model.PostalAddress,
                PostalAddressLine2 = model.PostalAddressLine2,
                PostalAddressCode = model.PostalAddressCode

            };

            db.CorrespondenceCustomers.Add(obj);
            db.SaveChanges();

            return obj.CustomerId;
        }

        // GET: Correspondences/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Correspondence correspondence = db.Correspondences.Find(id);
            if (correspondence == null)
            {
                return HttpNotFound();
            }
            ViewBag.CustomerId = new SelectList(db.CorrespondenceCustomers, "CustomerId", "CustomerName", correspondence.CustomerId);
            ViewBag.ReasonId = new SelectList(db.CorrespondenceReasons, "ReasonId", "CorrespondenceReasonName", correspondence.ReasonId);
            ViewBag.UnitId = new SelectList(db.MunicipalUnits, "UnitId", "MunicipalUnitName", correspondence.UnitId);
           
            return View(correspondence);
        }

        // POST: Correspondences/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CorrespondenceId,CustomerId,ReasonId,UnitId,Details,Documents")] Correspondence correspondence)
        {
            if (ModelState.IsValid)
            {
                db.Entry(correspondence).State = EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            ViewBag.CustomerId = new SelectList(db.CorrespondenceCustomers, "CustomerId", "CustomerName", correspondence.CustomerId);
            ViewBag.ReasonId = new SelectList(db.CorrespondenceReasons, "ReasonId", "CorrespondenceReasonName", correspondence.ReasonId);
            ViewBag.UnitId = new SelectList(db.MunicipalUnits, "UnitId", "MunicipalUnitName", correspondence.UnitId);
            return View(correspondence);
        }

        // GET: Correspondences/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Correspondence correspondence = db.Correspondences.Find(id);
            if (correspondence == null)
            {
                return HttpNotFound();
            }
            return View(correspondence);
        }

        // POST: Correspondences/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            Correspondence correspondence = db.Correspondences.Find(id);
            db.Correspondences.Remove(correspondence);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
