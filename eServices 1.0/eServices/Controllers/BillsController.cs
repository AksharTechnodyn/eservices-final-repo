﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using eServices.Models;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace eServices.Controllers
{
    public class BillsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Bills
        public ActionResult Index()
        {

            return View(db.Bills.ToList());
        }

        // GET: Bills/Details/5
        public ActionResult Details(string id,String monthYear)
        {

           
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (monthYear == null)
            {
                var ms = from i in db.Bills
                         where i.AccountNumber == id
                         select i.BillPeriod.Substring(3).Replace("-", "");
                int monthMax = 0;
                String newNumber = "";
                List<String> ls = new List<String>();
                foreach (var s in ms)
                {


                    String getMonth = s.Substring(0, 2);
                    newNumber = s.Remove(0, 2);
                    ls.Add(newNumber + getMonth);



                }
                //   var monthsString = db.Bills.ToList().Select(m => m.BillPeriod.Substring(3, 2));
                //var month = ms.Select(int.Parse).ToList();
                var month = ls.Select(int.Parse).ToList();
                foreach (var i in month)
                {
                    if (i > monthMax)
                    {
                        monthMax = i;
                    }
                }
                String year = monthMax.ToString().Substring(0, 4);
                String mth = monthMax.ToString().Substring(4);
                Bill bill = db.Bills.ToList().Find(m => m.AccountNumber == id && m.BillPeriod.Substring(6) == year && m.BillPeriod.Substring(3, 2) == mth);


                if (bill == null)
                {
                    return HttpNotFound();
                }
                return View(bill);
            }
            //Bill bill = db.Bills.ToList().Find(m => m.AccountNumber == id);
            //ViewBag.currentMonth = DateTime.Now.Month.ToString();

            //ViewBag.currentMonthName = String.Format("{0:MMMM}", DateTime.Now);

            string year1 = monthYear.Substring(2);
            string month1 = monthYear.Substring(0, 2);

            Bill bill1 = db.Bills.ToList().Find(m => m.AccountNumber == id && m.BillPeriod.Substring(6) == year1 && m.BillPeriod.Substring(3, 2) == month1);


            ViewBag.rand = "R";
            if (bill1 == null)
            {
                return HttpNotFound();
            }
            return View(bill1);
        }

        
        [HttpPost]
        public ActionResult Details()
        {

            var id = Request["AccountNumber"];
            var monthYear = Request["dropName"];

           
            

            
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            //if (monthYear == null)
            //{
            //    var ms = from i in db.Bills
            //             where i.AccountNumber == id
            //             select i.BillPeriod.Substring(3).Replace("/", "");
            //    int monthMax = 0;
            //    String newNumber = "";
            //    List<String> ls = new List<String>();
            //    foreach (var s in ms)
            //    {


            //        String getMonth = s.Substring(0, 2);
            //        newNumber = s.Remove(0, 2);
            //        ls.Add(newNumber + getMonth);



            //    }
            //    //   var monthsString = db.Bills.ToList().Select(m => m.BillPeriod.Substring(3, 2));
            //    //var month = ms.Select(int.Parse).ToList();
            //    var month = ls.Select(int.Parse).ToList();
            //    foreach (var i in month)
            //    {
            //        if (i > monthMax)
            //        {
            //            monthMax = i;
            //        }
            //    }
            //    String year = monthMax.ToString().Substring(0, 4);
            //    String mth = monthMax.ToString().Substring(4);
            //    Bill bill = db.Bills.ToList().Find(m => m.AccountNumber == id && m.BillPeriod.Substring(6) == year && m.BillPeriod.Substring(3, 2) == mth);


            //    if (bill == null)
            //    {
            //        return HttpNotFound();
            //    }
            //    return View(bill);
            //}
            //Bill bill = db.Bills.ToList().Find(m => m.AccountNumber == id);
            //ViewBag.currentMonth = DateTime.Now.Month.ToString();

            //ViewBag.currentMonthName = String.Format("{0:MMMM}", DateTime.Now);
            string year1;
            string month1;
            int monthnum;
            if (monthYear.Length == 5)
            {
                year1 = monthYear.Substring(1);
                monthnum = Convert.ToInt32(monthYear.Substring(0, 1)) + 1;
            }
            else
            {
                year1 = monthYear.Substring(2);
                monthnum = Convert.ToInt32(monthYear.Substring(0, 2)) + 1;

            }
             
             month1 = monthnum.ToString();//monthYear.Substring(0, 2);

            if (month1.Length == 1)
            {
                month1 = "0" + month1;
            }
            //if(month1 == "00")
            //{
            //    month1 = "12";
            //}

            Bill bill1 = db.Bills.ToList().Find(m => m.AccountNumber == id && m.BillPeriod.Substring(6) == year1 && m.BillPeriod.Substring(3, 2) == month1);

            ViewBag.setSelected = "test";
            ViewBag.rand = "R";
            if (bill1 == null)
            {
                TempData["errormsg"] = "There is no data for the selected period";
                return HttpNotFound();
                //return View();
            }
            else
            {
                return View(bill1);
            }
        }

        // GET: Bills/Charges/5
        public ActionResult Charges(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            return View(db.Charge.Where(m => m.BillId == id));
        }

        // GET: Bills/Create
        public ActionResult Create()
        {
            return View();
        }

        
        public ActionResult getBillPeriod(string id,string monthYear)
        {
           string year = monthYear.Substring(2);
            string month = monthYear.Substring(0, 2);

            Bill bill = db.Bills.ToList().Find(m => m.AccountNumber == id && m.BillPeriod.Substring(6) ==year && m.BillPeriod.Substring(3,2) == month);


           // return Json(new
           // {
           //     newUrl = Url.Action("Details", "Bills",new { id= bill.AccountNumber }) 
           // }
           //);


            //string billyear = bill.BillPeriod.Substring(6);
            //string billmonth = bill.BillPeriod.Substring(3,2);
            //return RedirectToAction("Details",bill);
            return Json(bill, JsonRequestBehavior.AllowGet);
        }

        // POST: Bills/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "BillId,InvoiceNumber,AccountHolderName,BillPeriod,AccountNumber,Details,PreviousBalance,PaymentMade,BalanceBroughtForward,TotalAmountDue")] Bill bill)
        {
            if (ModelState.IsValid)
            {
                db.Bills.Add(bill);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(bill);
        }

        // GET: Bills/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bill bill = db.Bills.Find(id);
            if (bill == null)
            {
                return HttpNotFound();
            }
            return View(bill);
        }

        // POST: Bills/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "BillId,InvoiceNumber,AccountHolderName,BillPeriod,AccountNumber,Details,PreviousBalance,PaymentMade,BalanceBroughtForward,TotalAmountDue")] Bill bill)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bill).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(bill);
        }

        // GET: Bills/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bill bill = db.Bills.Find(id);
            if (bill == null)
            {
                return HttpNotFound();
            }
            return View(bill);
        }

        // POST: Bills/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            Bill bill = db.Bills.Find(id);
            db.Bills.Remove(bill);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult CovertToPdf(string id)
        {
            var Bill = db.Bills.Find(id);
           EmailService es = new EmailService();
            es.StatementSend(Bill.AccountNumber);
            Document pdfDoc = new Document(PageSize.A4, 25, 25, 25, 15);
            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();

            if(Bill == null)
            {
                return Json("Null", JsonRequestBehavior.AllowGet);
            }
            //Top Heading
           

            //Horizontal Line
            Paragraph line = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 1)));
            //pdfDoc.Add(line);

            //Table
            PdfPTable table = new PdfPTable(2);
            table.WidthPercentage = 100;
            //0=Left, 1=Centre, 2=Right
            table.HorizontalAlignment = 0;
            table.SpacingBefore = 20f;
            table.SpacingAfter = 30f;

            //Cell no 1
            PdfPCell cell4 = new PdfPCell();
            cell4.Border = 0;

            //cell.AddElement(chunk);
            cell4.Colspan = 5;
            cell4.BackgroundColor = BaseColor.WHITE;

            Image image = Image.GetInstance(Server.MapPath("~/Content/pics/cropped-transparent_TECH_DATA1.png"));
            image.ScaleAbsolute(200, 150);
            cell4.AddElement(image);
            table.AddCell(cell4);

            //Cell no 2
        

            //Add table to document
            pdfDoc.Add(table);
            Chunk heading = new Chunk("Smart Bill", FontFactory.GetFont("Arial", 20, Element.ALIGN_RIGHT, BaseColor.BLUE));
            pdfDoc.Add(heading);

            //Horizontal Line
            //line = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 1)));
            //pdfDoc.Add(line);

            //Invoice Table
            table = new PdfPTable(2);
            table.WidthPercentage = 65;
            table.HorizontalAlignment = 0;
            table.SpacingBefore = 20f;
            table.SpacingAfter = 10f;

  

            table.AddCell("Invoice Number");
            table.AddCell(Bill.InvoiceNumber);
            table.AddCell("Name of Account Holder");
            table.AddCell(Bill.AccountHolderName);
            table.AddCell("Date Account Due");
            table.AddCell(Bill.DateAccountDue);
            table.AddCell("Bill Period");
            table.AddCell(Bill.BillPeriod);
            table.AddCell("Account Number");
            table.AddCell(Bill.AccountNumber);

            pdfDoc.Add(table);
            //Invoice Table Table

            //Property Valuation
            Chunk PropertyDiv = new Chunk("Propery Valuation", FontFactory.GetFont("Arial", 12, Element.ALIGN_CENTER, BaseColor.BLACK));
            PdfPTable headerprop = new PdfPTable(1);
            headerprop.WidthPercentage = 100;
            headerprop.HorizontalAlignment = 0;
            headerprop.SpacingBefore = 20f;
            headerprop.AddCell("Propery Valuation");
            headerprop.SpacingAfter = 10f;
            pdfDoc.Add(headerprop);

            PdfPTable propertytable = new PdfPTable(3);
            propertytable.WidthPercentage = 100;
            propertytable.HorizontalAlignment = 0;
            propertytable.SpacingAfter = 10f;

            propertytable.AddCell("Stand Size");
            propertytable.AddCell("Date of Valuation");
            propertytable.AddCell("Municipal Valuation");

            propertytable.AddCell(Bill.StandSize);
            propertytable.AddCell(Bill.DateofValuation);
            propertytable.AddCell(Bill.MunicipalValuation);

            pdfDoc.Add(propertytable);
            //Property Valuation



            //Summary of Account
            Chunk SummaryDiv = new Chunk("Summary of Account", FontFactory.GetFont("Arial", 12, Element.ALIGN_CENTER, BaseColor.BLACK));
            PdfPTable headersummary = new PdfPTable(1);
            headersummary.WidthPercentage = 100;
            headersummary.SpacingBefore = 20f;
            headersummary.SpacingAfter = 10f;
            headersummary.HorizontalAlignment = 0;
            headersummary.AddCell("Summary of Account");
            pdfDoc.Add(headersummary);

            PdfPTable summarytable = new PdfPTable(2);
            summarytable.WidthPercentage = 100;
            summarytable.HorizontalAlignment = 0;
            summarytable.SpacingAfter = 10f;
            summarytable.AddCell("Details");
            summarytable.AddCell("Amount");

            summarytable.AddCell("Previous Account Balance");
            summarytable.AddCell("R " + Bill.PreviousBalance);

            summarytable.AddCell("Payment Made (" +Bill.PaymentMade + ")");
            summarytable.AddCell("R "+ Bill.PaymentMadeAmount);

            summarytable.AddCell("Balance Brought Forward");
            summarytable.AddCell("R " + Bill.BalanceBroughtForward);

            summarytable.AddCell("Current Charges");
            summarytable.AddCell("R " + Bill.ChargesAmount);

            summarytable.AddCell("TOTAL AMOUNT DUE");
            summarytable.AddCell("R " + Bill.TotalAmountDue);

            pdfDoc.Add(summarytable);
            //Summary of Account

            //Charges
            PdfPTable chargesheader = new PdfPTable(1);
            chargesheader.WidthPercentage = 100;
            chargesheader.HorizontalAlignment = 0;
            chargesheader.SpacingBefore = 20f;
            chargesheader.AddCell("Current Charges");
            chargesheader.SpacingAfter = 10f;
            pdfDoc.Add(chargesheader);

            PdfPTable charges = new PdfPTable(2);
            charges.WidthPercentage = 100;
            charges.HorizontalAlignment = 0;
            charges.SpacingAfter = 10f;
            charges.AddCell("Item");
            charges.AddCell("Amount");
            foreach(var e in db.Charge.Where(x=>x.BillId==Bill.BillId))
            {
                charges.AddCell(e.ItemName);
                charges.AddCell("R " + e.Price);
            }
            pdfDoc.Add(charges);
            //Charges

            pdfWriter.CloseStream = false;
            pdfDoc.Close();
            Response.Buffer = true;
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=" + "Bill.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Write(pdfDoc);
            Response.End();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
