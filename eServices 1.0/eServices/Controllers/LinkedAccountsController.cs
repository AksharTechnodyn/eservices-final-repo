﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using eServices.Models;
using PagedList;
using Newtonsoft.Json;

namespace eServices.Controllers
{
    public class LinkedAccountsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        BusinessLogic bl = new BusinessLogic();
        
        // GET: LinkedAccounts
        public ActionResult Index()
        {
            List<moqLinkedAccount> linkedAccounts = db.moqLinkedAccounts.ToList();
            ViewBag.accList = new SelectList(linkedAccounts, "AccountId", "AccountName");

            return View(linkedAccounts.ToList());
        }

        // GET: LinkedAccounts/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LinkedAccount linkedAccount = db.LinkedAccounts.Find(id);
            if (linkedAccount == null)
            {
                return HttpNotFound();
            }
            return View(linkedAccount);
        }

        // GET: LinkedAccounts/Create
        [Authorize]
        public ActionResult Create(string id, string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "accountNo" : "";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
            ViewBag.StatusSortParam = String.IsNullOrEmpty(sortOrder) ? "status" : "";

            if (searchString != null)
            {
                page = 1;

            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            var customer = db.Customers.ToList().Find(x => x.Email == User.Identity.Name);
            List<LinkedAccount> accounts = db.LinkedAccounts.ToList().FindAll(x => x.CustomerId == customer.CustomerId && x.AccountStatus != "Rejected");
            ViewBag.Data = accounts;

            if (!String.IsNullOrEmpty(searchString))
            {
                var search = searchString.ToLower().ToString().Trim();
                accounts = accounts.Where(p => p.AccountNumber.ToLower().Contains(search) || p.AccountStatus.ToLower().Contains(search) || p.DateCreated.ToString().ToLower().Contains(search)).ToList();
            }

            switch (sortOrder)
            {
                case "accountNo":
                    accounts = accounts.OrderByDescending(c => c.AccountNumber).ToList();
                    break;
                case "status":
                    accounts = accounts.OrderByDescending(c => c.AccountStatus).ToList();
                    break;
                case "Date":
                    accounts = accounts.OrderByDescending(c => c.DateCreated).ToList();
                    break;
                default:
                    accounts = accounts.OrderBy(c => c.DateCreated).ToList();
                    break;
            }

            ViewBag.AccountTypeId = new SelectList(db.AccountTypes, "AccountTypeId", "AccountTypeName");
            ViewBag.CustomerId = new SelectList(db.Customers, "CustomerId", "CustomerName");

            int pageSize = 5;
            int pageNumber = (page ?? 1);
            return View(accounts.ToPagedList(pageNumber, pageSize));
        }


        public ActionResult Create2(string AccountNumber)
        {
            try
            {
                ViewBag.message = null;
                moqLinkedAccount moqLinkedAccount = db.moqLinkedAccounts.ToList().Find(x => x.AccountNumber == AccountNumber);

                var file = HttpContext.Request.Files["file"];
                if (moqLinkedAccount != null)
                {
                    LinkedAccount linkedAccount1 = db.LinkedAccounts.ToList().Find(x => x.AccountNumber == moqLinkedAccount.AccountNumber);
                    if (linkedAccount1 == null)
                    {
                        var docCheck = "";
                        if (file != null)
                        {
                            docCheck = file.FileName;
                        }

                        LinkedAccount newacc = new LinkedAccount()
                        {
                            AccountId = Guid.NewGuid().ToString(),
                            AccountNumber = AccountNumber,
                            AccountStatus = "Awaiting Verification",
                            AccountType = "RMS",
                            DateCreated = DateTime.Now,
                            CustomerId = bl.Customer(User.Identity.Name).CustomerId,
                            Documents = docCheck
                        };
                        db.LinkedAccounts.Add(newacc);
                        db.SaveChanges();


                        if (System.IO.Path.GetFileName(file.FileName) != "")
                        {
                            string doc = System.IO.Path.GetFileName(file.FileName);
                            string path = System.IO.Path.Combine(Server.MapPath("~/Content/LinkedAccountUploadsDoc"), doc);
                            file.SaveAs(path);

                            LinkedAccountUpload linkedUpload = new LinkedAccountUpload()
                            {
                                DocumentsId = Guid.NewGuid().ToString(),
                                CustomerId = bl.Customer(User.Identity.Name).CustomerId,
                                Documents = doc,
                                uploadDate = DateTime.Now,
                                AccountId = newacc.AccountId
                            };

                            db.LinkedAccountsUploads.Add(linkedUpload);
                            db.SaveChanges();
                        }

                        LinkedAccountEmail email = new LinkedAccountEmail()
                        {
                            LinkedEmail = User.Identity.Name,
                            AccountId = newacc.AccountId,
                            EmailId = Guid.NewGuid().ToString(),
                            EmailStatus = "Active"
                        };

                        db.LinkedAccountEmails.Add(email);
                        db.SaveChanges();

                        //var user = db.Customers.ToList().Find(x => x.Email == User.Identity.Name);
                        //Bill bil = new Bill
                        //{
                        //    BillId = Guid.NewGuid().ToString(),
                        //    AccountNumber = AccountNumber,
                        //    AccountHolderName = user.CustomerName + " " + user.CustomerSurname,
                        //    DateAccountDue = DateTime.Now.ToString(),
                        //    BillPeriod = DateTime.Now.ToString(),
                        //    PreviousBalance = "0",
                        //    PaymentMade = DateTime.Now.ToString(),
                        //    PaymentMadeAmount = "0",
                        //    BalanceBroughtForward = "0",
                        //    TotalAmountDue = "0",
                        //    ChargesAmount = "0",
                        //    StandSize = "0",
                        //    DateofValuation = DateTime.Now.ToString(),
                        //    MunicipalValuation = "0",
                        //    InvoiceNumber = "0"
                        //};
                        //db.Bills.Add(bil);
                        //db.SaveChanges();

                        return Json(linkedAccount1.AccountStatus, JsonRequestBehavior.AllowGet);
                    }
                    return Json(System.IO.Path.GetFileName(file.FileName), JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {

            }
            return RedirectToAction("Create");
        }





        [Authorize]
        public ActionResult Create1(string AccountNumber)  ///Andrews MEthod
        {

            ////var linkedAccount1 = db.LinkedAccounts.ToList().Find(x => x.AccountNumber == linkedAccount.AccountNumber);
            //LinkedAccount linkedAccount1 = db.LinkedAccounts.ToList().Find(x => x.AccountNumber == linkedAccount.AccountNumber);

            //if (linkedAccount1 != null && linkedAccount1.AccountStatus == "Rejected")
            //{
            //    //linkedAccount1.AccountStatus = "Awaiting Verification";
            //    //linkedAccount1.ReasonId = null;
            //    //db.Entry(linkedAccount1).State = EntityState.Modified;
            //    //db.SaveChanges();

            //    TempData["AlertMessage"] = "This account was rejected";
            //    return RedirectToAction("Create", "LinkedAccounts");
            //}
            //else if (linkedAccount1 != null)
            //{
            //    TempData["AlertMessage"] = "This account has already been linked";
            //    return RedirectToAction("Create", "LinkedAccounts");
            //}




            //if (linkedAccount1 == null)
            //{
            //    var docCheck = "";
            //    if (file != null)
            //    {
            //        docCheck = file.FileName;
            //    }

            //    LinkedAccount newacc = new LinkedAccount()
            //    {
            //        AccountId = Guid.NewGuid().ToString(),
            //        AccountNumber = linkedAccount.AccountNumber,
            //        AccountStatus = "Awaiting Verification",
            //        AccountType = "RMS",
            //        DateCreated = DateTime.Now,
            //        CustomerId = bl.Customer(User.Identity.Name).CustomerId,
            //        Documents = docCheck


            //    };

            //    db.LinkedAccounts.Add(newacc);
            //    db.SaveChanges();

            //    if (file != null)
            //    {
            //        string doc = System.IO.Path.GetFileName(file.FileName);
            //        string path = System.IO.Path.Combine(Server.MapPath("~/Content/LinkedAccountUploadsDoc"), doc);
            //        file.SaveAs(path);

            //        LinkedAccountUpload linkedUpload = new LinkedAccountUpload()
            //        {
            //            DocumentsId = Guid.NewGuid().ToString(),
            //            CustomerId = bl.Customer(User.Identity.Name).CustomerId,
            //            Documents = doc,
            //            uploadDate = DateTime.Now,
            //            AccountId = newacc.AccountId
            //        };

            //        db.LinkedAccountsUploads.Add(linkedUpload);
            //        db.SaveChanges();

            //    }

            //    LinkedAccountEmail email = new LinkedAccountEmail()
            //    {
            //        LinkedEmail = User.Identity.Name,
            //        AccountId = newacc.AccountId,
            //        EmailId = Guid.NewGuid().ToString()

            //    };

            //    db.LinkedAccountEmails.Add(email);
            //    db.SaveChanges();


            ViewBag.message = null;
            moqLinkedAccount moqLinkedAccount = db.moqLinkedAccounts.ToList().Find(x => x.AccountNumber == AccountNumber);

            var file = HttpContext.Request.Files["file"];
            if (moqLinkedAccount != null)
            {
                LinkedAccount linkedAccount1 = db.LinkedAccounts.ToList().Find(x => x.AccountNumber == moqLinkedAccount.AccountNumber);
                if (linkedAccount1 == null)
                {
                    var docCheck = "";
                    if (file != null)
                    {
                        docCheck = file.FileName;
                    }

                    LinkedAccount newacc = new LinkedAccount()
                    {
                        AccountId = Guid.NewGuid().ToString(),
                        AccountNumber = AccountNumber,
                        AccountStatus = "Awaiting Verification",
                        AccountType = "RMS",
                        DateCreated = DateTime.Now,
                        CustomerId = bl.Customer(User.Identity.Name).CustomerId,
                        Documents = docCheck
                    };
                    db.LinkedAccounts.Add(newacc);
                    db.SaveChanges();


                    if (System.IO.Path.GetFileName(file.FileName) != "")
                    {
                        string doc = System.IO.Path.GetFileName(file.FileName);
                        string path = System.IO.Path.Combine(Server.MapPath("~/Content/LinkedAccountUploadsDoc"), doc);
                        file.SaveAs(path);

                        LinkedAccountUpload linkedUpload = new LinkedAccountUpload()
                        {
                            DocumentsId = Guid.NewGuid().ToString(),
                            CustomerId = bl.Customer(User.Identity.Name).CustomerId,
                            Documents = doc,
                            uploadDate = DateTime.Now,
                            AccountId = newacc.AccountId
                        };

                        db.LinkedAccountsUploads.Add(linkedUpload);
                        db.SaveChanges();
                    }

                    LinkedAccountEmail email = new LinkedAccountEmail()
                    {
                        LinkedEmail = User.Identity.Name,
                        AccountId = newacc.AccountId,
                        EmailId = Guid.NewGuid().ToString(),
                        EmailStatus = "Active"
                    };

                    db.LinkedAccountEmails.Add(email);
                    db.SaveChanges();
                }
                    //-----------------------------------------------------//





                    Random r = new Random();
                for (int i = 0; i <= 12; i++)
                {
                    double prevBal = r.Next(4000, 5000);
                    double prevBal1 = r.Next(4000, 5000);
                    double payment = r.Next(3000, 4000);
                    double BalFwd = prevBal - payment;

                    //double e = r.Next(100, 500);
                    //double e1 = r.Next(100, 500);
                    double e2 = r.Next(100, 500);
                    double e3 = r.Next(100, 500);
                    double e4 = r.Next(100, 500);
                    double e5 = r.Next(100, 500);
                    double eVat = (e2 + e3 + e4 + e5) * 0.15;
                    double totalE = e2 + e3 + e4 + e5 + eVat;

                    double c = r.Next(240, 480);
                    double cVat = c * 0.15;
                    double totalC = c + cVat;

                    //double w = r.Next(300, 600);
                    //double w1 = r.Next(300, 600);
                    double w2 = r.Next(300, 600);
                    double w3 = r.Next(300, 600);
                    double w4 = r.Next(300, 600);
                    double wVat = (w2 + w3 + w4) * 0.15;
                    double totalW = w2 + w3 + w4 + wVat;


                    //double p = r.Next(600, 800);
                    double p1 = r.Next(600, 800);
                    double p2 = r.Next(600, 800);
                    double pVat = (p1 + p2) * 0.15;
                    double totalP = p1 + p2 + pVat;

                    double s = r.Next(20, 50);
                    double sVat = s * 0.15;
                    double totalS = s + sVat;

                    double TotalCharge = totalE + totalC + totalP + totalW + totalS;
                    double TotalAmtDue = BalFwd + TotalCharge;


                    int invNo = r.Next(1000, 10000);
                    int invNo1 = r.Next(1000, 10000);
                    int stand = r.Next(1, 10);
                    int MunVal = r.Next(500000, 800000);
                    var user = db.Customers.ToList().Find(x => x.Email == User.Identity.Name);

                    Bill bil = new Bill
                    {
                        BillId = Guid.NewGuid().ToString(),
                        AccountNumber = AccountNumber,
                        AccountHolderName = user.CustomerName + " " + user.CustomerSurname,
                        DateAccountDue = DateTime.Now.AddMonths(-i).Date.ToString("dd/MM/yyyy"),
                        BillPeriod = DateTime.Now.AddMonths(-i).ToString("dd/MM/yyyy"),
                        PreviousBalance = prevBal.ToString("0.00"), //prevBal.ToString("0.00"),
                        PaymentMade = DateTime.Now.AddMonths(-i).ToString("dd/MM/yyyy"),
                        PaymentMadeAmount = payment.ToString("0.00"),
                        BalanceBroughtForward = BalFwd.ToString("0.00"),
                        TotalAmountDue = TotalAmtDue.ToString("0.00"),
                        ChargesAmount = TotalCharge.ToString("0.00"),
                        StandSize = stand.ToString(),
                        DateofValuation = DateTime.Now.AddMonths(-i).ToString("dd/MM/yyyy"),
                        MunicipalValuation = MunVal.ToString("0.00"),
                        InvoiceNumber = "INV" + invNo.ToString()


                    };
                    db.Bills.Add(bil);
                    db.SaveChanges();

                    //---------------------Electricity---------------------------------------------------------------------------------------------------------



                    Charges elec = new Models.Charges
                    {
                        ChargesId = Guid.NewGuid().ToString(),
                        BillId = bil.BillId,
                        ItemName = "Meter Reading start of period",
                        ItemGroup = "Electricity",
                        //Price = e,
                        Date = DateTime.Now.AddMonths(-i).Date
                    };
                    db.Charge.Add(elec);
                    db.SaveChanges();


                    Charges elec1 = new Models.Charges
                    {
                        ChargesId = Guid.NewGuid().ToString(),
                        BillId = bil.BillId,
                        ItemName = "Meter Reading end of period",
                        ItemGroup = "Electricity",
                        //Price = e1,
                        Date = DateTime.Now.AddMonths(-i).Date
                    };
                    db.Charge.Add(elec1);
                    db.SaveChanges();




                    Charges elec2 = new Models.Charges
                    {
                        ChargesId = Guid.NewGuid().ToString(),
                        BillId = bil.BillId,
                        ItemName = "Units used 10 @ R5,00 per unit",
                        ItemGroup = "Electricity",
                        Price = e2,
                        Date = DateTime.Now.AddMonths(-i).Date
                    };
                    db.Charge.Add(elec2);
                    db.SaveChanges();



                    Charges elec3 = new Models.Charges
                    {
                        ChargesId = Guid.NewGuid().ToString(),
                        BillId = bil.BillId,
                        ItemName = "Demand side management levy",
                        ItemGroup = "Electricity",
                        Price = e3,
                        Date = DateTime.Now.AddMonths(-i).Date
                    };
                    db.Charge.Add(elec3);
                    db.SaveChanges();



                    Charges elec4 = new Models.Charges
                    {
                        ChargesId = Guid.NewGuid().ToString(),
                        BillId = bil.BillId,
                        ItemName = "Service Charge",
                        ItemGroup = "Electricity",
                        Price = e4,
                        Date = DateTime.Now.AddMonths(-i).Date
                    };
                    db.Charge.Add(elec4);
                    db.SaveChanges();


                    Charges elec5 = new Models.Charges
                    {
                        ChargesId = Guid.NewGuid().ToString(),
                        BillId = bil.BillId,
                        ItemName = "Network Charge",
                        ItemGroup = "Electricity",
                        Price = e5,
                        Date = DateTime.Now.AddMonths(-i).Date
                    };
                    db.Charge.Add(elec5);
                    db.SaveChanges();


                    Charges elec6 = new Models.Charges
                    {
                        ChargesId = Guid.NewGuid().ToString(),
                        BillId = bil.BillId,
                        ItemName = "VAT 15%",
                        ItemGroup = "Electricity",
                        Price = eVat,
                        Date = DateTime.Now.AddMonths(-i).Date
                    };
                    db.Charge.Add(elec6);
                    db.SaveChanges();








                    //------------------------------------------------------------------------------------------------------------




                    //---------------------Cleansing and Solid Waste--------------------------------------------------------------

                    Charges clean = new Models.Charges
                    {
                        ChargesId = Guid.NewGuid().ToString(),
                        BillId = bil.BillId,
                        ItemName = "Refusal Removal 1 bin @ R120,00",
                        ItemGroup = "Cleansing and Solid Waste",
                        Price = c,
                        Date = DateTime.Now.AddMonths(-i).Date
                    };
                    db.Charge.Add(clean);
                    db.SaveChanges();


                    Charges clean1 = new Models.Charges
                    {
                        ChargesId = Guid.NewGuid().ToString(),
                        BillId = bil.BillId,
                        ItemName = "VAT 15%",
                        ItemGroup = "Cleansing and Solid Waste",
                        Price = cVat,
                        Date = DateTime.Now.AddMonths(-i).Date
                    };
                    db.Charge.Add(clean1);
                    db.SaveChanges();


                    //------------------------------------------------------------------------------------------------------------------------


                    //---------------------Water and Sanitation---------------------------------------------------------------------------------

                    Charges water = new Models.Charges
                    {
                        ChargesId = Guid.NewGuid().ToString(),
                        BillId = bil.BillId,
                        ItemName = "Meter Reading start of period ",
                        ItemGroup = "Water and Sanitation",
                        //Price = w,
                        Date = DateTime.Now.AddMonths(-i).Date
                    };
                    db.Charge.Add(water);
                    db.SaveChanges();


                    Charges water1 = new Models.Charges
                    {
                        ChargesId = Guid.NewGuid().ToString(),
                        BillId = bil.BillId,
                        ItemName = "Meter Reading end of period",
                        ItemGroup = "Water and Sanitation",
                        //Price = w1,
                        Date = DateTime.Now.AddMonths(-i).Date

                    };
                    db.Charge.Add(water1);
                    db.SaveChanges();

                    Charges water2 = new Models.Charges
                    {
                        ChargesId = Guid.NewGuid().ToString(),
                        BillId = bil.BillId,
                        ItemName = "Units used 20 @ R1,00 per unit",
                        ItemGroup = "Water and Sanitation",
                        Price = w2,
                        Date = DateTime.Now.AddMonths(-i).Date
                    };
                    db.Charge.Add(water2);
                    db.SaveChanges();

                    Charges water3 = new Models.Charges
                    {
                        ChargesId = Guid.NewGuid().ToString(),
                        BillId = bil.BillId,
                        ItemName = "Sewer",
                        ItemGroup = "Water and Sanitation",
                        Price = w3,
                        Date = DateTime.Now.AddMonths(-i).Date

                    };
                    db.Charge.Add(water3);
                    db.SaveChanges();

                    Charges water4 = new Models.Charges
                    {
                        ChargesId = Guid.NewGuid().ToString(),
                        BillId = bil.BillId,
                        ItemName = "Sewer monthly charge based on stand size",
                        ItemGroup = "Water and Sanitation",
                        Price = w4,
                        Date = DateTime.Now.AddMonths(-i).Date
                    };
                    db.Charge.Add(water4);
                    db.SaveChanges();

                    Charges water5 = new Models.Charges
                    {
                        ChargesId = Guid.NewGuid().ToString(),
                        BillId = bil.BillId,
                        ItemName = "VAT 15%",
                        ItemGroup = "Water and Sanitation",
                        Price = wVat,
                        Date = DateTime.Now.AddMonths(-i).Date
                    };
                    db.Charge.Add(water5);
                    db.SaveChanges();

                    //---------------------------------------------------------------------------------------------------------------------------------------


                    //-----------------------------------Property--------------------------------------------------------------------------------------------



                    Charges prop = new Models.Charges
                    {
                        ChargesId = Guid.NewGuid().ToString(),
                        BillId = bil.BillId,
                        ItemName = "Category of Property: RESIDENTIAL A",
                        ItemGroup = "Property Rates",
                        //Price = p,
                        Date = DateTime.Now.AddMonths(-i).Date
                    };
                    db.Charge.Add(prop);
                    db.SaveChanges();




                    Charges prop1 = new Models.Charges
                    {
                        ChargesId = Guid.NewGuid().ToString(),
                        BillId = bil.BillId,
                        ItemName = "Property Rates",
                        ItemGroup = "Property Rates",
                        Price = p1,
                        Date = DateTime.Now.AddMonths(-i).Date
                    };
                    db.Charge.Add(prop1);
                    db.SaveChanges();



                    Charges prop2 = new Models.Charges
                    {
                        ChargesId = Guid.NewGuid().ToString(),
                        BillId = bil.BillId,
                        ItemName = "Less Subsidy",
                        ItemGroup = "Property Rates",
                        Price = p2,
                        Date = DateTime.Now.AddMonths(-i).Date
                    };
                    db.Charge.Add(prop2);
                    db.SaveChanges();


                    Charges prop3 = new Models.Charges
                    {
                        ChargesId = Guid.NewGuid().ToString(),
                        BillId = bil.BillId,
                        ItemName = "VAT 15%",
                        ItemGroup = "Property Rates",
                        Price = pVat,
                        Date = DateTime.Now.AddMonths(-i).Date
                    };
                    db.Charge.Add(prop3);
                    db.SaveChanges();


                    //-----------------------------------------------------------------------------------------------------------------------------------------


                    //---------------Sundries-------------------------------------------------------------------------------------------------------------------------


                    Charges sun = new Models.Charges
                    {
                        ChargesId = Guid.NewGuid().ToString(),
                        BillId = bil.BillId,
                        ItemName = "Miscellaneous Cost",
                        ItemGroup = "Sundries",
                        Price = s,
                        Date = DateTime.Now.AddMonths(-i).Date
                    };
                    db.Charge.Add(sun);
                    db.SaveChanges();

                    Charges sun1 = new Models.Charges
                    {
                        ChargesId = Guid.NewGuid().ToString(),
                        BillId = bil.BillId,
                        ItemName = "VAT 15%",
                        ItemGroup = "Sundries",
                        Price = sVat,
                        Date = DateTime.Now.AddMonths(-i).Date
                    };
                    db.Charge.Add(sun1);
                    db.SaveChanges();
                }

                //--------------------------------------------------------------------------------------------------------------------------------------

                //andrews code end

                //TempData["AlertMessage"] = "A request to link your account has been sent. We will notify you once your account has been linked or if your request has been rejected";

                return Json(AccountNumber, JsonRequestBehavior.AllowGet);
            }

            else
            {
                //TempData["AlertMessage"] = "A request to link your account has been sent. We will notify you once your account has been linked or if your request has been rejected";
                // return RedirectToAction("Create");
                return Json("", JsonRequestBehavior.AllowGet);

            }
        }


        /// <summary>
        
        /// </summary>
        /// <param name="AccountNumber"></param>
        /// <param name="Email"></param>
        /// <param name="action"></param>
        /// <returns></returns>


        public ActionResult LinkAccount(string AccountNumber, string Email, string action)
        {
            try
            {
                if (action == "Submit")
                {
                    return RedirectToAction("Create");
                }
                if (AccountNumber != null)
                {
                    LinkedAccount acc = db.LinkedAccounts.ToList().Find(x => x.AccountNumber == AccountNumber);

                    if (acc == null)
                    {
                        LinkedAccount newacc = new LinkedAccount()
                        {
                            AccountId = Guid.NewGuid().ToString(),
                            AccountNumber = AccountNumber,
                            AccountStatus = "Awaiting Verification",
                            AccountType = "RMS",
                            DateCreated = DateTime.Now,
                            CustomerId = bl.Customer(User.Identity.Name).CustomerId

                        };

                        db.LinkedAccounts.Add(newacc);
                        db.SaveChanges();

                        LinkedAccountEmail email = new LinkedAccountEmail()
                        {
                            LinkedEmail = Email,
                            AccountId = newacc.AccountId,
                            EmailId = Guid.NewGuid().ToString()

                        };

                        db.LinkedAccountEmails.Add(email);
                        db.SaveChanges();


                        var user = db.Customers.ToList().Find(x => x.Email == User.Identity.Name);
                        Bill bil = new Bill
                        {
                            BillId = Guid.NewGuid().ToString(),
                            AccountNumber = newacc.AccountNumber,
                            AccountHolderName = user.CustomerName + " " + user.CustomerSurname,
                            DateAccountDue = DateTime.Now.ToString(),
                            BillPeriod = DateTime.Now.ToString(),
                            PreviousBalance = "0",
                            PaymentMade = DateTime.Now.ToString(),
                            PaymentMadeAmount = "0",
                            BalanceBroughtForward = "0",
                            TotalAmountDue = "0",
                            ChargesAmount = "0",
                            StandSize = "0",
                            DateofValuation = DateTime.Now.ToString(),
                            MunicipalValuation = "0",
                            InvoiceNumber = "0"


                        };
                        db.Bills.Add(bil);
                        db.SaveChanges();



                    }
                    else
                    {
                        LinkedAccountEmail email = new LinkedAccountEmail()
                        {
                            LinkedEmail = Email,
                            AccountId = acc.AccountId,
                            EmailId = Guid.NewGuid().ToString()

                        };

                        db.LinkedAccountEmails.Add(email);
                        db.SaveChanges();
                    }

                }
                return Json(Email, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        // POST: LinkedAccounts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "AccountId,CustomerId,AccountType,AccountNumber,AccountStatus,DateCreated")] LinkedAccount linkedAccount)
        {
            if (ModelState.IsValid)
            {
                linkedAccount.AccountId = Guid.NewGuid().ToString();
                var customerid = db.Customers.ToList().Find(x => x.Email == User.Identity.Name).CustomerId;
                linkedAccount.CustomerId = customerid;
                linkedAccount.AccountStatus = "Awaiting Verification";
                linkedAccount.DateCreated = DateTime.Now;
                db.LinkedAccounts.Add(linkedAccount);

                var user = db.Customers.ToList().Find(x => x.Email == User.Identity.Name);
                Bill bil = new Bill
                {
                    BillId = Guid.NewGuid().ToString(),
                    AccountNumber = linkedAccount.AccountNumber,
                    AccountHolderName = user.CustomerName + " " + user.CustomerSurname,
                    DateAccountDue = DateTime.Now.ToString(),
                    BillPeriod = DateTime.Now.ToString(),
                    PreviousBalance = "0",
                    PaymentMade = DateTime.Now.ToString(),
                    PaymentMadeAmount = "0",
                    BalanceBroughtForward = "0",
                    TotalAmountDue = "0",
                    ChargesAmount = "0",
                    StandSize = "0",
                    DateofValuation = DateTime.Now.ToString(),
                    MunicipalValuation = "0",
                    InvoiceNumber = "0"


                };
                db.Bills.Add(bil);
                db.SaveChanges();


                if (Request["Email"] != null)
                {
                    LinkedAccountEmail email = new LinkedAccountEmail();
                    email.EmailId = Guid.NewGuid().ToString();
                    email.AccountId = linkedAccount.AccountId;
                    email.LinkedEmail = Request["Email"];
                    email.EmailStatus = "Active";
                    db.LinkedAccountEmails.Add(email);
                    db.SaveChanges();
                }
                TempData["AlertMessage"] = "A request to link your account has been sent. We will notify you once your account has been linked or if your request has been rejected";
                return RedirectToAction("Create");
            }

            //ViewBag.AccountTypeId = new SelectList(db.AccountTypes, "AccountTypeId", "AccountTypeName", linkedAccount.AccountTypeId);
            ViewBag.CustomerId = new SelectList(db.Customers, "CustomerId", "CustomerName", linkedAccount.CustomerId);
            return View(linkedAccount);
        }

        public ActionResult getAccount(string id)
        {
            LinkedAccount obj = db.LinkedAccounts.ToList().Find(m => m.AccountId == id);
            return Json(obj, JsonRequestBehavior.AllowGet);
        }


        public ActionResult getAccountNo(string acc)
        {
            moqLinkedAccount moqLinkedAccount = db.moqLinkedAccounts.ToList().Find(x => x.AccountNumber == acc);
            if (moqLinkedAccount != null)
            {
                var customer = db.Customers.ToList().Find(x => x.Email == User.Identity.Name);
                LinkedAccount obj = db.LinkedAccounts.ToList().Find(m => m.AccountNumber == moqLinkedAccount.AccountNumber && m.CustomerId == customer.CustomerId);
                string status = "";
                if (obj != null)
                {
                    status = obj.AccountStatus;
                }
                else
                {
                    status = "null";
                }
                return Json(status, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("NotFound", JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult getEmail(string Email)
        {
            string status = "";
            LinkedAccountEmail obj = db.LinkedAccountEmails.ToList().Find(m => m.LinkedEmail == Email);
            if (obj != null)
            {
                status = obj.EmailStatus;
            }
            else
            {
                status = "null";
            }
            return Json(status, JsonRequestBehavior.AllowGet);
        }

        public String getCustomerDetails()
        {

            var userName = db.Customers.ToList().Find(x => x.Email == User.Identity.Name).CustomerName;
            var userSurname = db.Customers.ToList().Find(x => x.Email == User.Identity.Name).CustomerName;
            //Customer cl = new Customer()
            //{
            //    CustomerName = userName,
            //    CustomerSurname = userSurname
            //};
            return userName + " " + userSurname;
        }


        public ActionResult UnlinkAccount(string AccountId)
        {
            bl.DeleteLinkedAccount(AccountId);
            TempData["AlertMessage"] = "Account Successfully Unlinked.";
            return RedirectToAction("Create");
        }

        public ActionResult RelinkAccount(string AccountId)
        {
            bl.RelinkAccount(AccountId);
            TempData["AlertMessage"] = "Account Successfully Linked.";
            return RedirectToAction("Create");
        }


        public ActionResult DeleteLinkEmail(string Email)
        {

            bl.DeleteEmail(Email);
            TempData["AlertMessage"] = "Email Successfully Unlinked.";
            return RedirectToAction("Create");
        }

        // GET: LinkedAccounts


        public ActionResult ManageEmails(string id)
        {
            var acc = db.LinkedAccounts.ToList().Find(x => x.AccountId == id);
            ViewBag.accid = acc.AccountNumber;
            return View(acc);
        }
        

        public ActionResult LinkEmail(string Email, string accno)
        {
            bl.AddEmail(accno, Email);
            var acc = db.LinkedAccountEmails.ToList().Find(x => x.AccountId == accno);
            ViewBag.accid = acc.EmailStatus;

            var obj = db.LinkedAccounts.ToList().Find(x => x.AccountId == accno);


            TempData["AlertMessage"] = "Email Successfully Linked.";

            return View("ManageEmails", obj);
        }

        public ActionResult ValidateEmail(string Email, string accno)
        {
            bool exist = false;
            var validate = db.LinkedAccounts.FirstOrDefault(x => x.AccountId == accno);
            if (validate != null)
            {
                var checkEmail = db.LinkedAccountEmails.FirstOrDefault(x => x.LinkedEmail == Email && x.LinkedAccount.AccountNumber == validate.AccountNumber);
                if (checkEmail != null)
                {
                    exist = true;
                    return Json(exist, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    exist = false;
                    return Json(exist, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(exist, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AddOrDelete(string AccountId, string Email, string command)
        {

            try
            {
                var accno = db.LinkedAccounts.ToList().Find(x => x.AccountId == AccountId);
                List<LinkedAccountEmail> email = db.LinkedAccountEmails.Where(x => x.AccountId == AccountId).ToList();

                var results = JsonConvert.SerializeObject(email, Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore });


                if (email != null)
                {
                    TempData["AlertMessage"] = "This Email And This Account Already Linked.";
                }
                else
                {
                    bl.AddEmail(accno.AccountId, Email);
                    var fetchData = db.LinkedAccountEmails.ToList().Where(x => x.AccountId == AccountId);
                    ViewBag.Data = fetchData;
                    TempData["AlertMessage"] = "Email Successfully Linked.";
                }
                return Json(new { results }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception)
            {
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists, see your system administrator.");

                return Json("Error", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult UnsubscribeEmail(string AccountNumber, string LinkedEmail)
        {
            try
            {
                var accno = db.LinkedAccounts.ToList().Find(x => x.AccountId == AccountNumber);
                LinkedAccountEmail email = db.LinkedAccountEmails.ToList().Find(x => x.LinkedEmail == LinkedEmail && x.AccountId == accno.AccountId);
                bl.DeleteLinkedEmail(accno.AccountId, LinkedEmail);
                TempData["AlertMessage"] = "Email Successfully Unlinked";
                return RedirectToAction("ManageEmails",new { id=accno.AccountId});
            }
            catch (Exception ex)
            {
                TempData["AlertMessage"] = ex.Message;
                return RedirectToAction("ManageEmails", new { id = AccountNumber });
            }
        }


        public ActionResult SubscribeEmail(string AccountNumber, string LinkedEmail)
        {
            try
            {
                var accno = db.LinkedAccounts.ToList().Find(x => x.AccountId == AccountNumber);
                LinkedAccountEmail email = db.LinkedAccountEmails.ToList().Find(x => x.LinkedEmail == LinkedEmail && x.AccountId == accno.AccountId);
                bl.ReLinkEmail(accno.AccountId, LinkedEmail);
                TempData["AlertMessage"] = "Email Successfully linked";
                return RedirectToAction("ManageEmails", new { id = accno.AccountId });
            }
            catch (Exception ex)
            {
                TempData["AlertMessage"] = ex.Message;
                return RedirectToAction("ManageEmails", new { id = AccountNumber });
            }
        }

        public ActionResult ViewBills()
        {

            return View();
        }

        // GET: LinkedAccounts/Edit/5
        public ActionResult Edit(string AccId)
        {
            if (AccId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LinkedAccount linkedAccount = db.LinkedAccounts.Find(AccId);
            if (linkedAccount == null)
            {
                return HttpNotFound();
            }
            //ViewBag.AccountTypeId = new SelectList(db.AccountTypes, "AccountTypeId", "AccountTypeName", linkedAccount.AccountTypeId);
            ViewBag.CustomerId = new SelectList(db.Customers, "CustomerId", "CustomerName", linkedAccount.CustomerId);
            return View(linkedAccount);
        }

        // POST: LinkedAccounts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "AccountId,CustomerId,AccountType,AccountNumber,AccountStatus,DateCreated")] LinkedAccount linkedAccount)
        {            
            if (ModelState.IsValid)
            {
                db.Entry(linkedAccount).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            //ViewBag.AccountTypeId = new SelectList(db.AccountTypes, "AccountTypeId", "AccountTypeName", linkedAccount.AccountTypeId);
            ViewBag.CustomerId = new SelectList(db.Customers, "CustomerId", "CustomerName", linkedAccount.CustomerId);
            return View(linkedAccount);
        }

        // GET: LinkedAccounts/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LinkedAccount linkedAccount = db.LinkedAccounts.Find(id);
            if (linkedAccount == null)
            {
                return HttpNotFound();
            }
            return View(linkedAccount);
        }

        // POST: LinkedAccounts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            LinkedAccount linkedAccount = db.LinkedAccounts.Find(id);
            db.LinkedAccounts.Remove(linkedAccount);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
