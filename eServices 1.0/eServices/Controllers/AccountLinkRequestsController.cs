﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using eServices.Models;
using System.Data.Entity.Validation;

namespace eServices.Controllers
{
    public class AccountLinkRequestsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: AccountLinkRequests
        public ActionResult Index()
        {
            var accountLinkRequests = db.LinkedAccounts.Include(a => a.Customer);
            return View(accountLinkRequests.ToList());
        }

        // GET: AccountLinkRequests/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AccountLinkRequest accountLinkRequest = db.AccountLinkRequests.Find(id);
            if (accountLinkRequest == null)
            {
                return HttpNotFound();
            }
            return View(accountLinkRequest);
        }

        // GET: AccountLinkRequests/Create
        public ActionResult Create(string id)
        {
            ViewBag.CustomerId = new SelectList(db.Customers, "CustomerId", "CustomerName");
            return View();
        }

        // POST: AccountLinkRequests/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "accountRequestID,CustomerId,AccountType,AccountNumber,AccountStatus,DateCreated")] AccountLinkRequest accountLinkRequest)
        {
            if (ModelState.IsValid)
            {
                accountLinkRequest.accountRequestID = Guid.NewGuid().ToString();
                var customerid = db.Customers.ToList().Find(x => x.Email == User.Identity.Name).CustomerId;
                accountLinkRequest.CustomerId = customerid;
                accountLinkRequest.AccountStatus = "Active";
                accountLinkRequest.DateCreated = DateTime.Now;
                db.AccountLinkRequests.Add(accountLinkRequest);
                db.SaveChanges();
                TempData["AlertMessage"] = "Account Link Request Sent.";
                return RedirectToAction("Create","LinkedAccounts");
            }

            ViewBag.CustomerId = new SelectList(db.Customers, "CustomerId", "CustomerName", accountLinkRequest.CustomerId);
            return View(accountLinkRequest);
        }

        [HttpGet]
        public ActionResult AcceptAcc(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AccountLinkRequest accountLinkRequest = db.AccountLinkRequests.Find(id);
            if (accountLinkRequest == null)
            {
                return HttpNotFound();
            }

            try
            {
                LinkedAccount la = new LinkedAccount()
                {
                    CustomerId = accountLinkRequest.CustomerId,
                    AccountNumber = accountLinkRequest.AccountNumber,
                    AccountStatus = accountLinkRequest.AccountStatus,
                    DateCreated = accountLinkRequest.DateCreated
                };

                db.LinkedAccounts.Add(la);
                db.SaveChanges();

            }
            catch (DbEntityValidationException e)
            {
                foreach (var validationErrors in e.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
            }
               
            





            return RedirectToAction("Index");
        }

        








        // GET: AccountLinkRequests/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AccountLinkRequest accountLinkRequest = db.AccountLinkRequests.Find(id);
            if (accountLinkRequest == null)
            {
                return HttpNotFound();
            }
            ViewBag.CustomerId = new SelectList(db.Customers, "CustomerId", "CustomerName", accountLinkRequest.CustomerId);
            return View(accountLinkRequest);
        }

        // POST: AccountLinkRequests/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "accountRequestID,CustomerId,AccountType,AccountNumber,AccountStatus,DateCreated")] AccountLinkRequest accountLinkRequest)
        {
            if (ModelState.IsValid)
            {
                db.Entry(accountLinkRequest).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CustomerId = new SelectList(db.Customers, "CustomerId", "CustomerName", accountLinkRequest.CustomerId);
            return View(accountLinkRequest);
        }

        // GET: AccountLinkRequests/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AccountLinkRequest accountLinkRequest = db.AccountLinkRequests.Find(id);
            if (accountLinkRequest == null)
            {
                return HttpNotFound();
            }
            return View(accountLinkRequest);
        }

        // POST: AccountLinkRequests/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            AccountLinkRequest accountLinkRequest = db.AccountLinkRequests.Find(id);
            db.AccountLinkRequests.Remove(accountLinkRequest);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
