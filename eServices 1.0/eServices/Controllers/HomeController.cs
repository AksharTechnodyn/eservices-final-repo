﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using eServices.Models;

namespace eServices.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        [Authorize]
        public ActionResult Index()
        {
            var customer = db.Customers.ToList().Find(m => m.Email == User.Identity.Name);
            ViewBag.Count = db.LinkedAccounts.Where(m => m.CustomerId == customer.CustomerId && m.AccountStatus=="Active").ToList().Count;
            ViewBag.accounts = db.LinkedAccounts.Where(m => m.CustomerId == customer.CustomerId && m.AccountStatus == "Active").ToList();

            if (ViewBag.Count ==1)
            {
            ViewBag.AccountNumber = db.LinkedAccounts.FirstOrDefault(m => m.CustomerId == customer.CustomerId && m.AccountStatus == "Active").AccountNumber;
            }
            var usertype = db.Customers.ToList().Find(x => x.Email == User.Identity.Name).RoleName;
            if (usertype == "Admin")
            {
                return RedirectToAction("Index", "AccountLinkRequests");
            }
            else
            {
                return View();
            }
               
        }


        public ActionResult getAccount(string id)
        {
            Bill obj = db.Bills.ToList().Find(m => m.AccountNumber == id);
            return Json(obj, JsonRequestBehavior.AllowGet);
        }


        public ActionResult Index2()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}