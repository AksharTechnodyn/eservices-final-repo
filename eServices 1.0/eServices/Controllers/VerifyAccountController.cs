﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using eServices.Models;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Configuration;
using Microsoft.AspNet.Identity;

namespace eServices.Controllers
{
    public class VerifyAccountController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: VerifyAccount
        public ActionResult Index()
        {
            var linkedAccounts = db.LinkedAccounts.Include(l => l.Customer);
            return View(linkedAccounts.ToList());
        }

        // GET: VerifyAccount/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LinkedAccount linkedAccount = db.LinkedAccounts.Find(id);
            if (linkedAccount == null)
            {
                return HttpNotFound();
            }
            return View(linkedAccount);
        }

        // GET: VerifyAccount/Create
        public ActionResult Create()
        {
            ViewBag.CustomerId = new SelectList(db.Customers, "CustomerId", "CustomerName");
            return View();
        }

        // POST: VerifyAccount/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "AccountId,CustomerId,AccountType,AccountNumber,AccountStatus,DateCreated")] LinkedAccount linkedAccount)
        {
            if (ModelState.IsValid)
            {
                db.LinkedAccounts.Add(linkedAccount);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CustomerId = new SelectList(db.Customers, "CustomerId", "CustomerName", linkedAccount.CustomerId);
            return View(linkedAccount);
        }

        // GET: VerifyAccount/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LinkedAccount linkedAccount = db.LinkedAccounts.Find(id);
            if (linkedAccount == null)
            {
                return HttpNotFound();
            }
            ViewBag.CustomerId = new SelectList(db.Customers, "CustomerId", "CustomerName", linkedAccount.CustomerId);
            ViewBag.ReasonId = new SelectList(db.RejectReasons, "ReasonId", "Description", linkedAccount.ReasonId);
            return View(linkedAccount);
        }

        // POST: VerifyAccount/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "AccountId,CustomerId,AccountType,ReasonId,AccountNumber,AccountStatus,DateCreated")] LinkedAccount linkedAccount, string command)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    bool approved = (command == "Approve");

                    if (approved)
                    {
                        linkedAccount.AccountStatus = "Active";
                        linkedAccount.DateCreated = DateTime.Now;
                        db.Entry(linkedAccount).State = EntityState.Modified;
                        db.SaveChanges();

                        var lnk = db.LinkedAccounts.ToList().Find(x => x.AccountId == linkedAccount.AccountId);
                        var cust = db.Customers.ToList().Find(x => x.CustomerId == lnk.CustomerId);
                        EmailService es = new EmailService();
                        es.Verify(cust.Email, lnk.AccountNumber, "Approved");

                    }
                    else
                    {
                        linkedAccount.AccountStatus = "Rejected";
                        linkedAccount.DateCreated = DateTime.Now;
                        db.Entry(linkedAccount).State = EntityState.Modified;
                        db.SaveChanges();
                        var lnk = db.LinkedAccounts.ToList().Find(x => x.AccountId == linkedAccount.AccountId);
                        var cust = db.Customers.ToList().Find(x => x.CustomerId == lnk.CustomerId);
                        var rs = db.RejectReasons.ToList().Find(x => x.ReasonId==lnk.ReasonId);
                        EmailService es = new EmailService();
                        es.VerifyReject(cust.Email, lnk.AccountNumber, "Rejected", rs.Description);

                    }
                    return RedirectToAction("Index","AccountLinkRequests");
                }
                catch (Exception ex)
                {
                    //Log the error (uncomment dex variable name and add a line here to write a log.
                    ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists, see your system administrator.");
                }
            }
            ViewBag.CustomerId = new SelectList(db.Customers, "CustomerId", "CustomerName", linkedAccount.CustomerId);
            ViewBag.ReasonId = new SelectList(db.RejectReasons, "ReasonId", "Description", linkedAccount.ReasonId);
            return View(linkedAccount);
        }

        // GET: VerifyAccount/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LinkedAccount linkedAccount = db.LinkedAccounts.Find(id);
            if (linkedAccount == null)
            {
                return HttpNotFound();
            }
            return View(linkedAccount);
        }

        // POST: VerifyAccount/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            LinkedAccount linkedAccount = db.LinkedAccounts.Find(id);
            db.LinkedAccounts.Remove(linkedAccount);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }        
    }
}
