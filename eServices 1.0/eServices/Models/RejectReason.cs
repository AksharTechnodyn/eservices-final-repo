﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eServices.Models
{
    public class RejectReason
    {

        [Key]
        public string ReasonId { get; set; }

        [Required, Display(Name = "Reject Reason")]
        public string Description { get; set; }

        public ICollection<LinkedAccount> LinkedAccounts { get; set; }
    }
}