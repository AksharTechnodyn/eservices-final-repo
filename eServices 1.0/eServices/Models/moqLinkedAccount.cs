﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace eServices.Models
{
    public class moqLinkedAccount
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string AccountId { get; set; }
        
        public string AccountType { get; set; }

        public string AccountName { get; set; }
        
        public string AccountNumber { get; set; }

        public string AccountStatus { get; set; }

        public DateTime? DateCreated { get; set; }

       
    }
}