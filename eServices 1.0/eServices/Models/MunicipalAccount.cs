﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eServices.Models
{
    public class MunicipalAccount
    {
        [Key]
        public string MunicipalAccountId { get; set; }

        [Required, Display(Name = "Account Number")]
        public string AccountNumber { get; set; }

        [Required, Display(Name = "Account Type")]
        public string AccountType { get; set; }


    }
}