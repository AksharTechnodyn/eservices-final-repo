﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eServices.Models
{
    public class MunicipalUnit
    {
        [Key]
        public string UnitId { get; set; }

        [Required, Display(Name = "Municipal Unit")]
        public string MunicipalUnitName { get; set; }

        public ICollection<Correspondence> Correspondences { get; set; }
    }
}