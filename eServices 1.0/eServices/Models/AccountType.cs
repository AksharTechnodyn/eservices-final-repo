﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eServices.Models
{
    public class AccountType
    {
        [Key]
        public string AccountTypeId { get; set; }

        [Required, Display(Name = "Account Type")]
        public string AccountTypeName { get; set; }

        public ICollection<LinkedAccount> LinkedAccounts { get; set; }
    }
}