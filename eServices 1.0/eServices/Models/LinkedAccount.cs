﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eServices.Models
{
    public class LinkedAccount
    {
        [Key]
        public string AccountId { get; set; }

        public Customer Customer { get; set; }
        public string CustomerId { get; set; }

        [Display(Name ="Account Type")]
        public string AccountType { get; set; }

        public RejectReason RejectReason { get; set; }
        public string ReasonId { get; set; }

        [Required]
        [Display(Name = "Account Number")]
        public string AccountNumber { get; set; }

        public string AccountStatus { get; set; }
        public DateTime DateCreated { get; set; }

        [DataType(DataType.Upload)]
        [Display(Name = "Upload Documents")]
        public string Documents { get; set; }

        public ICollection<LinkedAccountEmail> LinkedAccountEmails { get; set; }
    }
}