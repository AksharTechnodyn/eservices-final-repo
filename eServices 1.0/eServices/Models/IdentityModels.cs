﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace eServices.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {

        }

        public DbSet<moqLinkedAccount> moqLinkedAccounts { get; set; }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<CorrespondenceType> CorrespondenceTypes { get; set; }
        public DbSet<CorrespondenceReason> CorrespondenceReasons { get; set; }
        public DbSet<Correspondence> Correspondences { get; set; }
        public DbSet<MunicipalUnit> MunicipalUnits { get; set; }
        public DbSet<LinkedAccountEmail> LinkedAccountEmails { get; set; }

        public DbSet<CorrespondenceCustomer> CorrespondenceCustomers { get; set; }

        public DbSet<CorrespondenceDocument> CorrespondenceDocuments { get; set; }

        public DbSet<CustomerDocument> CustomerDocuments { get; set; }

        public DbSet<PropertyValuation> PropertyValuations { get; set; }

        public DbSet<Charges> Charge { get; set; }

        public DbSet<Bill> Bills { get; set; }


        public DbSet<RejectReason> RejectReasons { get; set; }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public System.Data.Entity.DbSet<eServices.Models.LinkedAccount> LinkedAccounts { get; set; }

        public System.Data.Entity.DbSet<eServices.Models.AccountType> AccountTypes { get; set; }

        public System.Data.Entity.DbSet<eServices.Models.AccountLinkRequest> AccountLinkRequests { get; set; }

        public System.Data.Entity.DbSet<eServices.Models.LinkedAccountUpload> LinkedAccountsUploads { get; set; }

    }
}