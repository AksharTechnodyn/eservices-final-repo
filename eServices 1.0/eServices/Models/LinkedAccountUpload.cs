﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eServices.Models
{
    public class LinkedAccountUpload
    {
        
            [Key]
            public string DocumentsId { get; set; }

            [Display(Name = "Created Date")]
            public DateTime uploadDate { get; set; }

            [Display(Name = "Upload Documents")]
            public string Documents { get; set; }

            public Customer Customer { get; set; }
            public string CustomerId { get; set; }


            public string AccountId { get; set; }

    }
}