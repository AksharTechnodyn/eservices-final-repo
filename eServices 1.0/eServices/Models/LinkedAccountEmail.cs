﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eServices.Models
{
    public class LinkedAccountEmail
    {
        [Key]
        public string EmailId { get; set; }

        public LinkedAccount LinkedAccount { get; set; }
        public string AccountId { get; set; }

        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        public string LinkedEmail { get; set; }
        public string EmailStatus { get; set; }
    }
}