﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace eServices.Models
{
    public class BusinessLogic
    {
        ApplicationDbContext db = new ApplicationDbContext();

        public List<RejectReason> LoadReasons()
        {
            return db.RejectReasons.ToList();
        }

        public List<moqLinkedAccount> loadMoqAccounts()
        {
            return db.moqLinkedAccounts.ToList();
        }


        public List<LinkedAccount> getLinkedAccounts()
        {
            return db.LinkedAccounts.ToList().FindAll(x => x.AccountStatus != "Rejected");
        }

        public List<LinkedAccount> CustomerAccounts(string Email)
        {
            var customer = db.Customers.ToList().Find(x => x.Email == Email);
            return db.LinkedAccounts.ToList().FindAll(x => x.CustomerId == customer.CustomerId);
        }

        public Customer Customer(string email)
        {
            return db.Customers.ToList().Find(x => x.Email == email);
        }

        public Customer getCustomerByPass(string AccountId)
        {
            Customer cust = new Customer();
            LinkedAccount lnk = db.LinkedAccounts.ToList().Find(x => x.AccountId == AccountId);
            cust = db.Customers.ToList().Find(x => x.CustomerId == lnk.CustomerId);

            return cust;
        }

        public string CustomerDoc(string CustomerId)
        {
            var doc = db.CustomerDocuments.ToList().Find(x => x.CustomerId == CustomerId).Documents;
            return doc;
        }


        public string CountActiveAccounts(string email)
        {
            Customer c = db.Customers.ToList().Find(x => x.Email == email);
            int count = db.LinkedAccounts.Where(x => x.CustomerId == c.CustomerId && x.AccountStatus == "Active").ToList().Count;
            if (count > 9)
            {
                return "+9";
            }
            return count.ToString();
        }

        public string CountCorrespondences(string email)
        {
            Customer c = db.Customers.ToList().Find(x => x.Email == email);
            int count = db.Correspondences.Where(x => x.CustomerId == c.CustomerId).ToList().Count;

            if (count > 9)
            {
                return "+9";
            }
            return count.ToString();

        }




        public string LinkedUploadsCustomerDoc(string AccountId)
        {
            var uploaddoc = "";
            try
            {
               uploaddoc = db.LinkedAccountsUploads.ToList().Find(x => x.AccountId == AccountId).Documents;
                
            }
            catch (Exception e)
            {
               
            }
            return uploaddoc;
        }

        public List<LinkedAccountEmail> CustomerAccountEmails (string AccountId)
        {
            var account = db.LinkedAccounts.ToList().Find(x => x.AccountId == AccountId);
            return db.LinkedAccountEmails.ToList().FindAll(x => x.AccountId == account.AccountId);
        }

        public List<LinkedAccountEmail> getLinkedAccountEmails(string AccountId)
        {
            return db.LinkedAccountEmails.ToList();
        }

        public LinkedAccount LinkedAccount(string AccountId)
        {
            return db.LinkedAccounts.ToList().Find(x => x.AccountId == AccountId);
        }

        public List<LinkedAccountEmail> LinkedAccountEmails(string AccountId, string EmailId, string Email)
        {
            var account = db.LinkedAccounts.ToList().Find(x => x.AccountId == AccountId);
            return db.LinkedAccountEmails.ToList().FindAll(x => x.AccountId == account.AccountId && x.EmailId == EmailId && x.LinkedEmail==Email);
        }

        public List<LinkedAccount> LinkedAccounts(string AccountId)
        {
            return db.LinkedAccounts.ToList().FindAll(x => x.AccountId == AccountId);
        }

        public void DeleteLinkedAccount(string AccountId)
        {
            List<LinkedAccountEmail> emails = db.LinkedAccountEmails.ToList().FindAll(x => x.AccountId == AccountId);
            db.LinkedAccountEmails.RemoveRange(emails);
            var account = db.LinkedAccounts.ToList().Find(x => x.AccountId == AccountId);
            account.AccountStatus = "Inactive";
            //db.LinkedAccounts.Remove(account);
            db.Entry(account).State = EntityState.Modified;
            db.SaveChanges();
        }

        public void RelinkAccount(string AccountId)
        {
            List<LinkedAccountEmail> emails = db.LinkedAccountEmails.ToList().FindAll(x => x.AccountId == AccountId);
            db.LinkedAccountEmails.RemoveRange(emails);
            var account = db.LinkedAccounts.ToList().Find(x => x.AccountId == AccountId);
            account.AccountStatus = "Active";
            //db.LinkedAccounts.Remove(account);
            db.Entry(account).State = EntityState.Modified;
            db.SaveChanges();
        }


        public void DeleteLinkedEmail(string AccountId, string LinkedEmail)
        {
            // LinkedAccountEmail email = db.LinkedAccountEmails.ToList().Find(x => x.AccountId==AccountId && x.LinkedEmail == LinkedEmail);
            foreach(var item in db.LinkedAccountEmails.Where(x => x.AccountId == AccountId).ToList())
            {
                if(item.EmailId==LinkedEmail)
                {
                    item.EmailStatus = "Inactive";
                    //db.LinkedAccountEmails.Remove(email);
                    db.Entry(item).State = EntityState.Modified;
                    db.SaveChanges();
                    break;
                }
            }
        }


        public void ReLinkEmail(string AccountId, string LinkedEmail)
        {
            // LinkedAccountEmail email = db.LinkedAccountEmails.ToList().Find(x => x.AccountId==AccountId && x.LinkedEmail == LinkedEmail);
            foreach (var item in db.LinkedAccountEmails.Where(x => x.AccountId == AccountId).ToList())
            {
                if (item.EmailId == LinkedEmail)
                {
                    item.EmailStatus = "Active";
                    //db.LinkedAccountEmails.Remove(email);
                    db.Entry(item).State = EntityState.Modified;
                    db.SaveChanges();
                    break;
                }
            }
        }

        public void DeleteEmail(string emailLink)
        {
            LinkedAccountEmail email = db.LinkedAccountEmails.ToList().Find(x => x.LinkedEmail == emailLink);
            db.LinkedAccountEmails.Remove(email);
            db.SaveChanges();
        }


        public void AddEmail(string AccountId, string Email)
        {
            LinkedAccountEmail obj = new LinkedAccountEmail()
            {
                EmailId = Guid.NewGuid().ToString(),
                LinkedEmail = Email,
                AccountId = AccountId,
                EmailStatus = "Active"
            };

            db.LinkedAccountEmails.Add(obj);
            db.SaveChanges();
        }

        public List<LinkedAccount> CustomerRequests()
        {

            return db.LinkedAccounts.ToList();
        }


        public String getNameSurname(String Email)
        {
            String fullname = "";
            var name = db.Customers.ToList().Find(x => x.Email == Email).CustomerName;
            var surname = db.Customers.ToList().Find(x => x.Email == Email).CustomerSurname;
            return fullname = name + " " + surname;
        }



        //public String getTypeofId(string Email) 
        //{
        //    var id = db.Customers.ToList().Find(x => x.Email == Email).IdNumber;
        //    var pass = db.Customers.ToList().Find(x => x.Email == Email).PassportNumber;

        //    //var customer = db.LinkedAccounts.ToList().Find(x => x.AccountId ==);
        //    //var id = db.Customers.ToList().Find(x=>x.CustomerId == customer.CustomerId).IdNumber;
        //    //var pass = db.Customers.ToList().Find(x => x.CustomerId == customer.CustomerId).PassportNumber;
        //    if (id != null)
        //    {
        //        return id;
        //    }
        //    else
        //    {
        //        return pass;
        //    }

        //}

        //public LinkedAccount LinkedAccount(string AccNo)
        //{
        //    return db.LinkedAccounts.ToList().Find(x =>x.AccountNumber== AccNo);
        //}

        /*public void getLinkedAcc(string accId)
        {
            var accNo = db.LinkedAccounts.ToList().Find(x => x.AccountId == accId).AccountNumber;
            var accStatus = db.LinkedAccounts.ToList().Find(x => x.AccountId == accId).AccountStatus;
            //var accEmails = db.LinkedAccountEmails.ToList().Find(x => x.AccountId == accId).LinkedEmail.ToList();
            LinkedAccount acc = new LinkedAccount()
            {
                AccountNumber = accNo,
                AccountStatus = accStatus,

            };

        }*/

    }
}