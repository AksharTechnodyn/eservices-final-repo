﻿using eServices.Validation;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace eServices.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class CorrespondenceViewModel
    {
        [Required, Display(Name = "Name")]
        public string CustomerName { get; set; }

        [Required, Display(Name = "Surname")]
        public string CustomerSurname { get; set; }

        [Required, Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [StringLength(13, MinimumLength = 13, ErrorMessage = "Invalid Id")]
        [Display(Name = "Id Number")]
        public string IdNumber { get; set; }

        [Display(Name = "Passport Number")]
        public string PassportNumber { get; set; }

        [StringLength(10, MinimumLength = 10, ErrorMessage = "Invalid Phone number")]
        [Required, Display(Name = "Cellphone Number")]
        public string CellphoneNumber { get; set; }

        [Required, Display(Name = "Physical Address")]
        public string PhysicalAddress { get; set; }

        public string PhysicalAddressLine2 { get; set; }

        [StringLength(4, MinimumLength = 4, ErrorMessage = "Invalid Postal Code")]
        [Required, Display(Name = "Postal Code")]
        public string PhysicalPostalCode { get; set; }

        [Required, Display(Name = "Postal Address")]
        public string PostalAddress { get; set; }

        public string PostalAddressLine2 { get; set; }

        [StringLength(4, MinimumLength = 4, ErrorMessage = "Invalid Postal Code")]
        [Required, Display(Name = "Postal Code")]
        public string PostalAddressCode { get; set; }

        public CorrespondenceCustomer CorrespondenceCustomer { get; set; }
        public string CustomerId { get; set; }

        public CorrespondenceReason CorrespondenceReason { get; set; }
        [Display(Name = "Reason for Correspondence")]
        public string ReasonId { get; set; }

        public CorrespondenceType CorrespondenceType { get; set; }
        [Display(Name = "Correspondence Type")]
        public string TypeId { get; set; }


        public MunicipalUnit MunicipalUnit { get; set; }
        [Display(Name = "Municipal Unit")]
        public string UnitId { get; set; }

        [Required, Display(Name = "Details")]
        public string Details { get; set; }

        [DataType(DataType.Upload)]
        [Display(Name = "Upload Documents")]
        public string Documents { get; set; }
    }


    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
       

        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required, Display(Name = "Name")]
        [StringLength(10, MinimumLength = 3, ErrorMessage = "Invalid Input")]
        public string CustomerName { get; set; }

        [Required, Display(Name = "Surname")]
        [StringLength(10, MinimumLength = 3, ErrorMessage = "Invalid Input")]
        public string CustomerSurname { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
                
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required, Display(Name = "Type of ID")]
        public string TypeofId { get; set; }

        [RSAIDNumber(ErrorMessage = "A valid RSA ID Number is required.")]
        [StringLength(13, MinimumLength = 13, ErrorMessage = "Invalid ID ")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "ID must consist of numbers only!")]
        [Display(Name = "ID Number")]
        public string IdNumber { get; set; }


        [Display(Name = "Passport Number")]
        [RegularExpression("^[a-zA-Z0-9]*$", ErrorMessage = "Passport number must consist of numbers and letters only!")]
        public string PassportNumber { get; set; }

        [Required, Display(Name = "Gender")]
        public string Gender { get; set; }

        [StringLength(10, MinimumLength = 10, ErrorMessage = "Phone number must be 10 digits")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Invalid Phone number")]
        [Required, Display(Name = "Cellphone Number")]
        public string CellphoneNumber { get; set; }

        [Required, Display(Name = "Physical Address")]
        public string PhysicalAddress { get; set; }

        public string PhysicalAddressLine2 { get; set; }

        [StringLength(4, MinimumLength = 4, ErrorMessage = "Invalid Postal Code")]
        [Required, Display(Name = "Postal Code")]
        public string PhysicalPostalCode { get; set; }

        public string PostalAddress { get; set; }

        public string PostalAddressLine2 { get; set; }

        [StringLength(4, MinimumLength = 4, ErrorMessage = "Invalid Postal Code")]
        [Required, Display(Name = "Postal Code")]
        public string PostalAddressCode { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}
