﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eServices.Models
{
    public class CorrespondenceType
    {
        [Key]
        public string TypeId { get; set; }

        [Required, Display(Name = "Reason for Correspondence")]
        public string CorrespondenceTypeName { get; set; }

        public ICollection<Correspondence> Correspondences { get; set; }

    }
}