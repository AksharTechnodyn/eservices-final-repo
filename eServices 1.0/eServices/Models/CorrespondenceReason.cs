﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eServices.Models
{
    public class CorrespondenceReason
    {
        [Key]
        public string ReasonId { get; set; }

        [Required, Display(Name = "Reason for Correspondence")]
        public string CorrespondenceReasonName { get; set; }

        public ICollection<Correspondence> Correspondences { get; set; }
    }
}