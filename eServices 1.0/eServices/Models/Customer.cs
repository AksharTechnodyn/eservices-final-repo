﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using eServices.Validation;

namespace eServices.Models
{
    public class Customer
    {
        [Key]
        public string CustomerId { get; set; }

        [Required, Display(Name = "Name")]
        public string CustomerName { get; set; }

        [Required, Display(Name = "Surname")]
        public string CustomerSurname { get; set; }

        [Required, Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Display(Name = "Type of Id")]
        public string TypeofId { get; set; }

        [RSAIDNumber(ErrorMessage = "A valid RSA ID Number is required.")]
        [StringLength(13, MinimumLength = 13, ErrorMessage = "Invalid Id")]
        [Display(Name = "Id Number")]
        public string IdNumber { get; set; }


        [Display(Name = "Passport Number")]
        public string PassportNumber { get; set; }

        [Required, Display(Name = "Gender")]
        public string Gender { get; set; }

        [StringLength(10, MinimumLength = 10, ErrorMessage = "Invalid Phone number")]
        [Required, Display(Name = "Cellphone Number")]
        public string CellphoneNumber { get; set; }

        [Required, Display(Name = "Physical Address")]
        public string PhysicalAddress { get; set; }

        public string PhysicalAddressLine2 { get; set; }

        [StringLength(4, MinimumLength = 4, ErrorMessage = "Invalid Postal Code")]
        [Required, Display(Name = "Postal Code")]
        public string PhysicalPostalCode { get; set; }

        [Required, Display(Name = "Postal Address")]
        public string PostalAddress { get; set; }

        public string PostalAddressLine2 { get; set; }

        [StringLength(4, MinimumLength = 4, ErrorMessage = "Invalid Postal Code")]
        [Required, Display(Name = "Postal Code")]
        public string PostalAddressCode { get; set; }

        public string RoleName { get; set; }

        
        public ICollection<CustomerDocument> CustomerDocuments { get; set; }

        public ICollection<LinkedAccount> LinkedAccounts { get; set; }
    }


    public class CustomerDocument
    {
        [Key]
        public string DocumentsId { get; set; }

        [Display(Name = "Created Date")]
        public DateTime uploadDate { get; set; }

        [Display(Name = "Upload Documents")]
        public string Documents { get; set; }

        public Customer Customer { get; set; }
        public string CustomerId { get; set; }
    }


    public class Bill
    {
        public string BillId { get; set; }

        [Required, Display(Name = "Invoice Number")]
        public string InvoiceNumber { get; set; }

        [Required, Display(Name = "Name of Account Holder")]
        public string AccountHolderName { get; set; }

        [DataType(DataType.Date)]
        [Required, Display(Name = "Date Account Due")]
        public string DateAccountDue { get; set; }

        [Required, Display(Name = "Bill Period")]
        public string BillPeriod { get; set; }

        [Required, Display(Name = "Account Number")]
        public string AccountNumber { get; set; }

        [Required, Display(Name = "Previous Account Balance")]
        public string PreviousBalance { get; set; }

        [Required, Display(Name = "Payment Made")]
        public string PaymentMade { get; set; }

        public string PaymentMadeAmount { get; set; }

        public string ChargesAmount { get; set; }

        [Required, Display(Name = "Balance Brought Forward")]
        public string BalanceBroughtForward { get; set; }

        [Required, Display(Name = "Stand Size")]
        public string StandSize { get; set; }

        [Required, Display(Name = "Date of Valuation")]
        public string DateofValuation { get; set; }

        [Required, Display(Name = "Municipal Valuation")]
        public string MunicipalValuation { get; set; }

        [Required, Display(Name = "Total Amount Due")]
        public string TotalAmountDue { get; set; }

        public ICollection<Charges> Charges { get; set; }
        public ICollection<PropertyValuation> PropertyValuations { get; set; }

    }

    public class BL
    {
        private ApplicationDbContext db = new ApplicationDbContext();



        public List<Charges> getCharges(String id)
        {
            return db.Charge.Where(m => m.BillId == id).ToList();
        }

        public List<PropertyValuation> getPropertyValuation(String id)
        {
            return db.PropertyValuations.Where(m => m.BillId == id).ToList();
        }
    }

    public class Charges
    {
        [Key]
        public string ChargesId { get; set; }

        public Bill Bill { get; set; }
        public string BillId { get; set; }

        [Required, Display(Name = "Item Name")]
        public string ItemName { get; set; }

        public string ItemGroup { get; set; }

        [DisplayFormat(DataFormatString = "{0:C2}")]
        [Required, Display(Name = "Price")]
        public double Price { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Created Date")]
        public DateTime Date { get; set; }

    }

    public class PropertyValuation
    {
        [Key]
        public string ChargesId { get; set; }

        public Bill Bill { get; set; }
        public string BillId { get; set; }

        [Required, Display(Name = "Stand Size")]
        public string StandSize { get; set; }

        [Required, Display(Name = "Date of Valuation")]
        public string DateofValuation { get; set; }

        [Required, Display(Name = "Municipal Valuation")]
        public string MunicipalValuation { get; set; }

        [Display(Name = "Created Date")]
        public DateTime Date { get; set; }
    }
}