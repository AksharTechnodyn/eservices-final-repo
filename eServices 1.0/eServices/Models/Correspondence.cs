﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eServices.Models
{
    public class Correspondence
    {
        [Key]
        public string CorrespondenceId { get; set; }


        public CorrespondenceCustomer CorrespondenceCustomer { get; set; }
        public string CustomerId { get; set; }

        public CorrespondenceDocument CorrespondenceDocument { get; set; }
        public string DocumentId { get; set; }

        public CorrespondenceType CorrespondenceType { get; set; }
        public string TypeId { get; set; }

        public CorrespondenceReason CorrespondenceReason { get; set; }
        public string ReasonId { get; set; }

        public MunicipalUnit MunicipalUnit { get; set; }
        public string UnitId { get; set; }

        [Required, Display(Name = "Details")]
        public string Details { get; set; }
    }
    public class CorrespondenceDocument
    {
        [Key]
        public string DocumentsId { get; set; }

        [Display(Name = "Created Date")]
        public DateTime uploadDate { get; set; }

        [DataType(DataType.Upload)]
        [Display(Name = "Upload Documents")]
        public string Documents { get; set; }
    }

    public class CorrespondenceCustomer
    {
        [Key]
        public string CustomerId { get; set; }

        [Required, Display(Name = "Name")]
        public string CustomerName { get; set; }

        [Required, Display(Name = "Surname")]
        public string CustomerSurname { get; set; }

        [Required, Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [StringLength(13, MinimumLength = 13, ErrorMessage = "Invalid Id")]
        [Display(Name = "Id Number")]
        public string IdNumber { get; set; }

        [Display(Name = "Passport Number")]
        public string PassportNumber { get; set; }

        [StringLength(10, MinimumLength = 10, ErrorMessage = "Invalid Phone number")]
        [Required, Display(Name = "Cellphone Number")]
        public string CellphoneNumber { get; set; }

        [Required, Display(Name = "Physical Address")]
        public string PhysicalAddress { get; set; }

        public string PhysicalAddressLine2 { get; set; }

        [StringLength(4, MinimumLength = 4, ErrorMessage = "Invalid Postal Code")]
        [Required, Display(Name = "Postal Code")]
        public string PhysicalPostalCode { get; set; }

        [Required, Display(Name = "Postal Address")]
        public string PostalAddress { get; set; }

        public string PostalAddressLine2 { get; set; }

        [StringLength(4, MinimumLength = 4, ErrorMessage = "Invalid Postal Code")]
        [Required, Display(Name = "Postal Code")]
        public string PostalAddressCode { get; set; }

    }
}