﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace eServices.Models
{
    public class AccountLinkRequest
    {
        [Key]
        public String accountRequestID { get; set;}

        [Display(Name ="Account Holder")]
        public Customer Customer { get; set; }
        public string CustomerId { get; set; }

        [Display(Name = "Account Type")]
        public string AccountType { get; set; }

        [Required]
        [Display(Name = "Account Number")]
        public string AccountNumber { get; set; }

        public string AccountStatus { get; set; }
        public DateTime DateCreated { get; set; }

    }
}